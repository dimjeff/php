<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* shop.html.twig */
class __TwigTemplate_c7bf972bf29cc5081166befffd25cea99bfef23402a91f7f15bcaf1259bcc13c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "﻿<!DOCTYPE html>
<html lang=\"en\">
<head>
\t<title>";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo " - Day 06 eShop</title>
\t";
        // line 5
        $this->displayBlock('head', $context, $blocks);
        // line 6
        echo "\t<meta charset=\"utf-8\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<meta name=\"description\" content=\"OneTech shop project\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/styles/bootstrap4/bootstrap.min.css\">
\t<link href=\"/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css\" rel=\"stylesheet\" type=\"text/css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/plugins/OwlCarousel2-2.2.1/owl.carousel.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/plugins/OwlCarousel2-2.2.1/owl.theme.default.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/plugins/OwlCarousel2-2.2.1/animate.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/plugins/jquery-ui-1.12.1.custom/jquery-ui.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/styles/shop_styles.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/styles/shop_responsive.css\">

</head>

<body>

<div class=\"super_container\">
\t
\t<!-- Header -->
\t
\t<header class=\"header\">

\t\t<!-- Top Bar -->

\t\t<div class=\"top_bar\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col d-flex flex-row\">
\t\t\t\t\t\t<div class=\"top_bar_contact_item\"><div class=\"top_bar_icon\"><img src=\"/images/phone.png\" alt=\"\"></div>+38 068 005 3570</div>
\t\t\t\t\t\t<div class=\"top_bar_contact_item\"><div class=\"top_bar_icon\"><img src=\"/images/mail.png\" alt=\"\"></div><a href=\"mailto:fastsales@gmail.com\">fastsales@gmail.com</a></div>
\t\t\t\t\t\t<div class=\"top_bar_content ml-auto\">
\t\t\t\t\t\t\t<div class=\"top_bar_menu\">
\t\t\t\t\t\t\t\t<ul class=\"standard_dropdown top_bar_dropdown\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">English<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Italian</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Spanish</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Japanese</a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">\$ US dollar<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">EUR Euro</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">GBP British Pound</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">JPY Japanese Yen</a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"top_bar_user\">
\t\t\t\t\t\t\t\t<div class=\"user_icon\"><img src=\"/images/user.svg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t";
        // line 60
        if (($context["userSession"] ?? null)) {
            // line 61
            echo "\t\t\t\t\t\t\t\t<div><span _ngcontent-twg-c0=\"\" class=\"hdca-button--label-right\"> Hi ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userSession"] ?? null), "name", [], "any", false, false, false, 61), "html", null, true);
            echo " | <a href=\"/logout\">Logout</a> </span></div>
\t\t\t\t\t\t\t\t";
        } else {
            // line 63
            echo "\t\t\t\t\t\t\t\t<div><a href=\"/register\">Register</a></div>
\t\t\t\t\t\t\t\t<div><a href=\"/login\">Sign in</a></div>
\t\t\t\t\t\t\t\t";
        }
        // line 65
        echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>\t\t
\t\t</div>

\t\t<!-- Header Main -->

\t\t<div class=\"header_main\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">

\t\t\t\t\t<!-- Logo -->
\t\t\t\t\t<div class=\"col-lg-2 col-sm-3 col-3 order-1\">
\t\t\t\t\t\t<div class=\"logo_container\">
\t\t\t\t\t\t\t<div class=\"logo\"><a href=\"#\">OneTech</a></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<!-- Search -->
\t\t\t\t\t<div class=\"col-lg-6 col-12 order-lg-2 order-3 text-lg-left text-right\">
\t\t\t\t\t\t<div class=\"header_search\">
\t\t\t\t\t\t\t<div class=\"header_search_content\">
\t\t\t\t\t\t\t\t<div class=\"header_search_form_container\">
\t\t\t\t\t\t\t\t\t<form action=\"#\" class=\"header_search_form clearfix\">
\t\t\t\t\t\t\t\t\t\t<input type=\"search\" required=\"required\" class=\"header_search_input\" placeholder=\"Search for products...\">
\t\t\t\t\t\t\t\t\t\t<div class=\"custom_dropdown\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"custom_dropdown_list\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"custom_dropdown_placeholder clc\">All Categories</span>
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-chevron-down\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"custom_list clc\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">All Categories</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">Computers</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">Laptops</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">Cameras</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">Hardware</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">Smartphones</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"header_search_button trans_300\" value=\"Submit\"><img src=\"/images/search.png\" alt=\"\"></button>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<!-- Wishlist -->
\t\t\t\t\t<div class=\"col-lg-4 col-9 order-lg-3 order-2 text-lg-left text-right\">
\t\t\t\t\t\t<div class=\"wishlist_cart d-flex flex-row align-items-center justify-content-end\">
\t\t\t\t\t\t\t<div class=\"wishlist d-flex flex-row align-items-center justify-content-end\">
\t\t\t\t\t\t\t\t<div class=\"wishlist_icon\"><img src=\"/images/heart.png\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"wishlist_content\">
\t\t\t\t\t\t\t\t\t<div class=\"wishlist_text\"><a href=\"#\">Wishlist</a></div>
\t\t\t\t\t\t\t\t\t<div class=\"wishlist_count\">115</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Cart -->
\t\t\t\t\t\t\t<div class=\"cart\">
\t\t\t\t\t\t\t\t<div class=\"cart_container d-flex flex-row align-items-center justify-content-end\">
\t\t\t\t\t\t\t\t\t<div class=\"cart_icon\">
\t\t\t\t\t\t\t\t\t\t<img src=\"/images/cart.png\" alt=\"\">
\t\t\t\t\t\t\t\t\t\t<div class=\"cart_count\"><span>10</span></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"cart_content\">
\t\t\t\t\t\t\t\t\t\t<div class=\"cart_text\"><a href=\"#\">Cart</a></div>
\t\t\t\t\t\t\t\t\t\t<div class=\"cart_price\">\$85</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t
\t\t<!-- Main Navigation -->

\t\t<nav class=\"main_nav\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"main_nav_content d-flex flex-row\">

\t\t\t\t\t\t\t<!-- Categories Menu -->

\t\t\t\t\t\t\t<div class=\"cat_menu_container\">
\t\t\t\t\t\t\t\t<div class=\"cat_menu_title d-flex flex-row align-items-center justify-content-start\">
\t\t\t\t\t\t\t\t\t<div class=\"cat_burger\"><span></span><span></span><span></span></div>
\t\t\t\t\t\t\t\t\t<div class=\"cat_menu_text\">categories</div>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<ul class=\"cat_menu\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Computers & Laptops <i class=\"fas fa-chevron-right ml-auto\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Cameras & Photos<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li class=\"hassubs\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Hardware<i class=\"fas fa-chevron-right\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li class=\"hassubs\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a>
\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Smartphones & Tablets<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">TV & Audio<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Gadgets<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Car Electronics<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Video Games & Consoles<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Accessories<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Main Nav Menu -->

\t\t\t\t\t\t\t<div class=\"main_nav_menu ml-auto\">
\t\t\t\t\t\t\t\t<ul class=\"standard_dropdown main_nav_dropdown\">
\t\t\t\t\t\t\t\t\t<li><a href=\"/\">Home<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li class=\"hassubs\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Super Deals<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li class=\"hassubs\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Featured Brands<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li class=\"hassubs\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Pages<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"shop.html\">Shop<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"product.html\">Product<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"blog.html\">Blog<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"blog_single.html\">Blog Post<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"regular.html\">Regular Post<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"cart.html\">Cart<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"contact.html\">Contact<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li><a href=\"blog.html\">Blog<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"contact.html\">Contact<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Menu Trigger -->

\t\t\t\t\t\t\t<div class=\"menu_trigger_container ml-auto\">
\t\t\t\t\t\t\t\t<div class=\"menu_trigger d-flex flex-row align-items-center justify-content-end\">
\t\t\t\t\t\t\t\t\t<div class=\"menu_burger\">
\t\t\t\t\t\t\t\t\t\t<div class=\"menu_trigger_text\">menu</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"cat_burger menu_burger_inner\"><span></span><span></span><span></span></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</nav>
\t\t
\t\t<!-- Menu -->

\t\t<div class=\"page_menu\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"page_menu_content\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"page_menu_search\">
\t\t\t\t\t\t\t\t<form action=\"#\">
\t\t\t\t\t\t\t\t\t<input type=\"search\" required=\"required\" class=\"page_menu_search_input\" placeholder=\"Search for products...\">
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<ul class=\"page_menu_nav\">
\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">Language<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">English<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Italian<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Spanish<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Japanese<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">Currency<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">US Dollar<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">EUR Euro<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">GBP British Pound<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">JPY Japanese Yen<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item\">
\t\t\t\t\t\t\t\t\t<a href=\"/\">Home<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">Super Deals<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Super Deals<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">Featured Brands<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Featured Brands<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">Trending Styles<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Trending Styles<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item\"><a href=\"blog.html\">blog<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item\"><a href=\"contact.html\">contact<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"menu_contact\">
\t\t\t\t\t\t\t\t<div class=\"menu_contact_item\"><div class=\"menu_contact_icon\"><img src=\"/images/phone_white.png\" alt=\"\"></div>+38 068 005 3570</div>
\t\t\t\t\t\t\t\t<div class=\"menu_contact_item\"><div class=\"menu_contact_icon\"><img src=\"/images/mail_white.png\" alt=\"\"></div><a href=\"mailto:fastsales@gmail.com\">fastsales@gmail.com</a></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t</header>
\t
\t<!-- Home -->

\t<div class=\"home\">
\t\t<div class=\"home_background parallax-window\" data-parallax=\"scroll\" data-image-src=\"/images/shop_background.jpg\"></div>
\t\t<div class=\"home_overlay\"></div>
\t\t<div class=\"home_content d-flex flex-column align-items-center justify-content-center\">
\t\t\t<h2 class=\"home_title\">";
        // line 355
        echo twig_escape_filter($this->env, ($context["pagetitle"] ?? null), "html", null, true);
        echo "</h2>
\t\t</div>
\t</div>

\t<!-- Shop -->

\t<div class=\"shop\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t
\t\t\t\t<div class=\"col-lg-12\">
                        ";
        // line 366
        $this->displayBlock('content', $context, $blocks);
        // line 367
        echo "\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<!-- Recently Viewed -->

\t<div class=\"viewed\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<div class=\"viewed_title_container\">
\t\t\t\t\t\t<h3 class=\"viewed_title\">Recently Viewed</h3>
\t\t\t\t\t\t<div class=\"viewed_nav_container\">
\t\t\t\t\t\t\t<div class=\"viewed_nav viewed_prev\"><i class=\"fas fa-chevron-left\"></i></div>
\t\t\t\t\t\t\t<div class=\"viewed_nav viewed_next\"><i class=\"fas fa-chevron-right\"></i></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"viewed_slider_container\">
\t\t\t\t\t\t
\t\t\t\t\t\t<!-- Recently Viewed Slider -->

\t\t\t\t\t\t<div class=\"owl-carousel owl-theme viewed_slider\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item discount d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_1.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$225<span>\$300</span></div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">Beoplay H7</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_2.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$379</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">LUNA Smartphone</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_3.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$225</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">Samsung J730F...</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item is_new d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_4.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$379</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">Huawei MediaPad...</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item discount d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_5.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$225<span>\$300</span></div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">Sony PS4 Slim</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_6.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$375</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">Speedlink...</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<!-- Brands -->

\t<div class=\"brands\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<div class=\"brands_slider_container\">
\t\t\t\t\t\t
\t\t\t\t\t\t<!-- Brands Slider -->

\t\t\t\t\t\t<div class=\"owl-carousel owl-theme brands_slider\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_1.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_2.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_3.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_4.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_5.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_6.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_7.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_8.jpg\" alt=\"\"></div></div>

\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<!-- Brands Slider Navigation -->
\t\t\t\t\t\t<div class=\"brands_nav brands_prev\"><i class=\"fas fa-chevron-left\"></i></div>
\t\t\t\t\t\t<div class=\"brands_nav brands_next\"><i class=\"fas fa-chevron-right\"></i></div>

\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<!-- Newsletter -->

\t<div class=\"newsletter\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<div class=\"newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center\">
\t\t\t\t\t\t<div class=\"newsletter_title_container\">
\t\t\t\t\t\t\t<div class=\"newsletter_icon\"><img src=\"/images/send.png\" alt=\"\"></div>
\t\t\t\t\t\t\t<div class=\"newsletter_title\">Sign up for Newsletter</div>
\t\t\t\t\t\t\t<div class=\"newsletter_text\"><p>...and receive %20 coupon for first shopping.</p></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"newsletter_content clearfix\">
\t\t\t\t\t\t\t<form action=\"#\" class=\"newsletter_form\">
\t\t\t\t\t\t\t\t<input type=\"email\" class=\"newsletter_input\" required=\"required\" placeholder=\"Enter your email address\">
\t\t\t\t\t\t\t\t<button class=\"newsletter_button\">Subscribe</button>
\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t<div class=\"newsletter_unsubscribe_link\"><a href=\"#\">unsubscribe</a></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<!-- Footer -->

\t<footer class=\"footer\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">

\t\t\t\t<div class=\"col-lg-3 footer_col\">
\t\t\t\t\t<div class=\"footer_column footer_contact\">
\t\t\t\t\t\t<div class=\"logo_container\">
\t\t\t\t\t\t\t<div class=\"logo\"><a href=\"#\">OneTech</a></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"footer_title\">Got Question? Call Us 24/7</div>
\t\t\t\t\t\t<div class=\"footer_phone\">+38 068 005 3570</div>
\t\t\t\t\t\t<div class=\"footer_contact_text\">
\t\t\t\t\t\t\t<p>17 Princess Road, London</p>
\t\t\t\t\t\t\t<p>Grester London NW18JR, UK</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"footer_social\">
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fab fa-facebook-f\"></i></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fab fa-twitter\"></i></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fab fa-youtube\"></i></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fab fa-google\"></i></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fab fa-vimeo-v\"></i></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-2 offset-lg-2\">
\t\t\t\t\t<div class=\"footer_column\">
\t\t\t\t\t\t<div class=\"footer_title\">Find it Fast</div>
\t\t\t\t\t\t<ul class=\"footer_list\">
\t\t\t\t\t\t\t<li><a href=\"#\">Computers & Laptops</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Cameras & Photos</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Hardware</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Smartphones & Tablets</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">TV & Audio</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<div class=\"footer_subtitle\">Gadgets</div>
\t\t\t\t\t\t<ul class=\"footer_list\">
\t\t\t\t\t\t\t<li><a href=\"#\">Car Electronics</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-2\">
\t\t\t\t\t<div class=\"footer_column\">
\t\t\t\t\t\t<ul class=\"footer_list footer_list_2\">
\t\t\t\t\t\t\t<li><a href=\"#\">Video Games & Consoles</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Accessories</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Cameras & Photos</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Hardware</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Computers & Laptops</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-2\">
\t\t\t\t\t<div class=\"footer_column\">
\t\t\t\t\t\t<div class=\"footer_title\">Customer Care</div>
\t\t\t\t\t\t<ul class=\"footer_list\">
\t\t\t\t\t\t\t<li><a href=\"#\">My Account</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Order Tracking</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Wish List</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Customer Services</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Returns / Exchange</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">FAQs</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Product Support</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>
\t\t</div>
\t</footer>

\t<!-- Copyright -->

\t<div class=\"copyright\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start\">
\t\t\t\t\t\t<div class=\"copyright_content\"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> by <a href=\"https://colorlib.com\" target=\"_blank\">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>
\t\t\t\t\t\t<div class=\"logos ml-sm-auto\">
\t\t\t\t\t\t\t<ul class=\"logos_list\">
\t\t\t\t\t\t\t\t<li><a href=\"#\"><img src=\"/images/logos_1.png\" alt=\"\"></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><img src=\"/images/logos_2.png\" alt=\"\"></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><img src=\"/images/logos_3.png\" alt=\"\"></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><img src=\"/images/logos_4.png\" alt=\"\"></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<script src=\"/js/jquery-3.3.1.min.js\"></script>
<script src=\"/styles/bootstrap4/popper.js\"></script>
<script src=\"/styles/bootstrap4/bootstrap.min.js\"></script>
<script src=\"/plugins/greensock/TweenMax.min.js\"></script>
<script src=\"/plugins/greensock/TimelineMax.min.js\"></script>
<script src=\"/plugins/scrollmagic/ScrollMagic.min.js\"></script>
<script src=\"/plugins/greensock/animation.gsap.min.js\"></script>
<script src=\"/plugins/greensock/ScrollToPlugin.min.js\"></script>
<script src=\"/plugins/OwlCarousel2-2.2.1/owl.carousel.js\"></script>
<script src=\"/plugins/easing/easing.js\"></script>
<script src=\"/plugins/Isotope/isotope.pkgd.min.js\"></script>
<script src=\"/plugins/jquery-ui-1.12.1.custom/jquery-ui.js\"></script>
<script src=\"/plugins/parallax-js-master/parallax.min.js\"></script>
<script src=\"/js/shop_custom.js\"></script>
</body>

</html>";
    }

    // line 4
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Default";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 366
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "shop.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  745 => 366,  739 => 5,  732 => 4,  428 => 367,  426 => 366,  412 => 355,  120 => 65,  115 => 63,  109 => 61,  107 => 60,  51 => 6,  49 => 5,  45 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("﻿<!DOCTYPE html>
<html lang=\"en\">
<head>
\t<title>{% block title %}Default{% endblock %} - Day 06 eShop</title>
\t{% block head %}{% endblock %}
\t<meta charset=\"utf-8\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<meta name=\"description\" content=\"OneTech shop project\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/styles/bootstrap4/bootstrap.min.css\">
\t<link href=\"/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css\" rel=\"stylesheet\" type=\"text/css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/plugins/OwlCarousel2-2.2.1/owl.carousel.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/plugins/OwlCarousel2-2.2.1/owl.theme.default.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/plugins/OwlCarousel2-2.2.1/animate.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/plugins/jquery-ui-1.12.1.custom/jquery-ui.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/styles/shop_styles.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/styles/shop_responsive.css\">

</head>

<body>

<div class=\"super_container\">
\t
\t<!-- Header -->
\t
\t<header class=\"header\">

\t\t<!-- Top Bar -->

\t\t<div class=\"top_bar\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col d-flex flex-row\">
\t\t\t\t\t\t<div class=\"top_bar_contact_item\"><div class=\"top_bar_icon\"><img src=\"/images/phone.png\" alt=\"\"></div>+38 068 005 3570</div>
\t\t\t\t\t\t<div class=\"top_bar_contact_item\"><div class=\"top_bar_icon\"><img src=\"/images/mail.png\" alt=\"\"></div><a href=\"mailto:fastsales@gmail.com\">fastsales@gmail.com</a></div>
\t\t\t\t\t\t<div class=\"top_bar_content ml-auto\">
\t\t\t\t\t\t\t<div class=\"top_bar_menu\">
\t\t\t\t\t\t\t\t<ul class=\"standard_dropdown top_bar_dropdown\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">English<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Italian</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Spanish</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Japanese</a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">\$ US dollar<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">EUR Euro</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">GBP British Pound</a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">JPY Japanese Yen</a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"top_bar_user\">
\t\t\t\t\t\t\t\t<div class=\"user_icon\"><img src=\"/images/user.svg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t{% if userSession %}
\t\t\t\t\t\t\t\t<div><span _ngcontent-twg-c0=\"\" class=\"hdca-button--label-right\"> Hi {{userSession.name}} | <a href=\"/logout\">Logout</a> </span></div>
\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t<div><a href=\"/register\">Register</a></div>
\t\t\t\t\t\t\t\t<div><a href=\"/login\">Sign in</a></div>
\t\t\t\t\t\t\t\t{% endif %}\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>\t\t
\t\t</div>

\t\t<!-- Header Main -->

\t\t<div class=\"header_main\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">

\t\t\t\t\t<!-- Logo -->
\t\t\t\t\t<div class=\"col-lg-2 col-sm-3 col-3 order-1\">
\t\t\t\t\t\t<div class=\"logo_container\">
\t\t\t\t\t\t\t<div class=\"logo\"><a href=\"#\">OneTech</a></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<!-- Search -->
\t\t\t\t\t<div class=\"col-lg-6 col-12 order-lg-2 order-3 text-lg-left text-right\">
\t\t\t\t\t\t<div class=\"header_search\">
\t\t\t\t\t\t\t<div class=\"header_search_content\">
\t\t\t\t\t\t\t\t<div class=\"header_search_form_container\">
\t\t\t\t\t\t\t\t\t<form action=\"#\" class=\"header_search_form clearfix\">
\t\t\t\t\t\t\t\t\t\t<input type=\"search\" required=\"required\" class=\"header_search_input\" placeholder=\"Search for products...\">
\t\t\t\t\t\t\t\t\t\t<div class=\"custom_dropdown\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"custom_dropdown_list\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"custom_dropdown_placeholder clc\">All Categories</span>
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fas fa-chevron-down\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"custom_list clc\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">All Categories</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">Computers</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">Laptops</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">Cameras</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">Hardware</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"clc\" href=\"#\">Smartphones</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"header_search_button trans_300\" value=\"Submit\"><img src=\"/images/search.png\" alt=\"\"></button>
\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<!-- Wishlist -->
\t\t\t\t\t<div class=\"col-lg-4 col-9 order-lg-3 order-2 text-lg-left text-right\">
\t\t\t\t\t\t<div class=\"wishlist_cart d-flex flex-row align-items-center justify-content-end\">
\t\t\t\t\t\t\t<div class=\"wishlist d-flex flex-row align-items-center justify-content-end\">
\t\t\t\t\t\t\t\t<div class=\"wishlist_icon\"><img src=\"/images/heart.png\" alt=\"\"></div>
\t\t\t\t\t\t\t\t<div class=\"wishlist_content\">
\t\t\t\t\t\t\t\t\t<div class=\"wishlist_text\"><a href=\"#\">Wishlist</a></div>
\t\t\t\t\t\t\t\t\t<div class=\"wishlist_count\">115</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Cart -->
\t\t\t\t\t\t\t<div class=\"cart\">
\t\t\t\t\t\t\t\t<div class=\"cart_container d-flex flex-row align-items-center justify-content-end\">
\t\t\t\t\t\t\t\t\t<div class=\"cart_icon\">
\t\t\t\t\t\t\t\t\t\t<img src=\"/images/cart.png\" alt=\"\">
\t\t\t\t\t\t\t\t\t\t<div class=\"cart_count\"><span>10</span></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"cart_content\">
\t\t\t\t\t\t\t\t\t\t<div class=\"cart_text\"><a href=\"#\">Cart</a></div>
\t\t\t\t\t\t\t\t\t\t<div class=\"cart_price\">\$85</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t
\t\t<!-- Main Navigation -->

\t\t<nav class=\"main_nav\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"main_nav_content d-flex flex-row\">

\t\t\t\t\t\t\t<!-- Categories Menu -->

\t\t\t\t\t\t\t<div class=\"cat_menu_container\">
\t\t\t\t\t\t\t\t<div class=\"cat_menu_title d-flex flex-row align-items-center justify-content-start\">
\t\t\t\t\t\t\t\t\t<div class=\"cat_burger\"><span></span><span></span><span></span></div>
\t\t\t\t\t\t\t\t\t<div class=\"cat_menu_text\">categories</div>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<ul class=\"cat_menu\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Computers & Laptops <i class=\"fas fa-chevron-right ml-auto\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Cameras & Photos<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li class=\"hassubs\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Hardware<i class=\"fas fa-chevron-right\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li class=\"hassubs\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a>
\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Smartphones & Tablets<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">TV & Audio<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Gadgets<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Car Electronics<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Video Games & Consoles<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Accessories<i class=\"fas fa-chevron-right\"></i></a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Main Nav Menu -->

\t\t\t\t\t\t\t<div class=\"main_nav_menu ml-auto\">
\t\t\t\t\t\t\t\t<ul class=\"standard_dropdown main_nav_dropdown\">
\t\t\t\t\t\t\t\t\t<li><a href=\"/\">Home<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li class=\"hassubs\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Super Deals<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li class=\"hassubs\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Featured Brands<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li class=\"hassubs\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Pages<i class=\"fas fa-chevron-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"shop.html\">Shop<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"product.html\">Product<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"blog.html\">Blog<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"blog_single.html\">Blog Post<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"regular.html\">Regular Post<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"cart.html\">Cart<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"contact.html\">Contact<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li><a href=\"blog.html\">Blog<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"contact.html\">Contact<i class=\"fas fa-chevron-down\"></i></a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Menu Trigger -->

\t\t\t\t\t\t\t<div class=\"menu_trigger_container ml-auto\">
\t\t\t\t\t\t\t\t<div class=\"menu_trigger d-flex flex-row align-items-center justify-content-end\">
\t\t\t\t\t\t\t\t\t<div class=\"menu_burger\">
\t\t\t\t\t\t\t\t\t\t<div class=\"menu_trigger_text\">menu</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"cat_burger menu_burger_inner\"><span></span><span></span><span></span></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</nav>
\t\t
\t\t<!-- Menu -->

\t\t<div class=\"page_menu\">
\t\t\t<div class=\"container\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col\">
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"page_menu_content\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"page_menu_search\">
\t\t\t\t\t\t\t\t<form action=\"#\">
\t\t\t\t\t\t\t\t\t<input type=\"search\" required=\"required\" class=\"page_menu_search_input\" placeholder=\"Search for products...\">
\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<ul class=\"page_menu_nav\">
\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">Language<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">English<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Italian<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Spanish<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Japanese<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">Currency<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">US Dollar<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">EUR Euro<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">GBP British Pound<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">JPY Japanese Yen<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item\">
\t\t\t\t\t\t\t\t\t<a href=\"/\">Home<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">Super Deals<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Super Deals<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">Featured Brands<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Featured Brands<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item has-children\">
\t\t\t\t\t\t\t\t\t<a href=\"#\">Trending Styles<i class=\"fa fa-angle-down\"></i></a>
\t\t\t\t\t\t\t\t\t<ul class=\"page_menu_selection\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Trending Styles<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Menu Item<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item\"><a href=\"blog.html\">blog<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t\t<li class=\"page_menu_item\"><a href=\"contact.html\">contact<i class=\"fa fa-angle-down\"></i></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"menu_contact\">
\t\t\t\t\t\t\t\t<div class=\"menu_contact_item\"><div class=\"menu_contact_icon\"><img src=\"/images/phone_white.png\" alt=\"\"></div>+38 068 005 3570</div>
\t\t\t\t\t\t\t\t<div class=\"menu_contact_item\"><div class=\"menu_contact_icon\"><img src=\"/images/mail_white.png\" alt=\"\"></div><a href=\"mailto:fastsales@gmail.com\">fastsales@gmail.com</a></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t</header>
\t
\t<!-- Home -->

\t<div class=\"home\">
\t\t<div class=\"home_background parallax-window\" data-parallax=\"scroll\" data-image-src=\"/images/shop_background.jpg\"></div>
\t\t<div class=\"home_overlay\"></div>
\t\t<div class=\"home_content d-flex flex-column align-items-center justify-content-center\">
\t\t\t<h2 class=\"home_title\">{{pagetitle}}</h2>
\t\t</div>
\t</div>

\t<!-- Shop -->

\t<div class=\"shop\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t
\t\t\t\t<div class=\"col-lg-12\">
                        {% block content %}{% endblock %}
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<!-- Recently Viewed -->

\t<div class=\"viewed\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<div class=\"viewed_title_container\">
\t\t\t\t\t\t<h3 class=\"viewed_title\">Recently Viewed</h3>
\t\t\t\t\t\t<div class=\"viewed_nav_container\">
\t\t\t\t\t\t\t<div class=\"viewed_nav viewed_prev\"><i class=\"fas fa-chevron-left\"></i></div>
\t\t\t\t\t\t\t<div class=\"viewed_nav viewed_next\"><i class=\"fas fa-chevron-right\"></i></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"viewed_slider_container\">
\t\t\t\t\t\t
\t\t\t\t\t\t<!-- Recently Viewed Slider -->

\t\t\t\t\t\t<div class=\"owl-carousel owl-theme viewed_slider\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item discount d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_1.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$225<span>\$300</span></div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">Beoplay H7</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_2.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$379</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">LUNA Smartphone</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_3.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$225</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">Samsung J730F...</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item is_new d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_4.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$379</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">Huawei MediaPad...</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item discount d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_5.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$225<span>\$300</span></div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">Sony PS4 Slim</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<!-- Recently Viewed Item -->
\t\t\t\t\t\t\t<div class=\"owl-item\">
\t\t\t\t\t\t\t\t<div class=\"viewed_item d-flex flex-column align-items-center justify-content-center text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"viewed_image\"><img src=\"/images/view_6.jpg\" alt=\"\"></div>
\t\t\t\t\t\t\t\t\t<div class=\"viewed_content text-center\">
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_price\">\$375</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"viewed_name\"><a href=\"#\">Speedlink...</a></div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<ul class=\"item_marks\">
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_discount\">-25%</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"item_mark item_new\">new</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<!-- Brands -->

\t<div class=\"brands\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<div class=\"brands_slider_container\">
\t\t\t\t\t\t
\t\t\t\t\t\t<!-- Brands Slider -->

\t\t\t\t\t\t<div class=\"owl-carousel owl-theme brands_slider\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_1.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_2.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_3.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_4.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_5.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_6.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_7.jpg\" alt=\"\"></div></div>
\t\t\t\t\t\t\t<div class=\"owl-item\"><div class=\"brands_item d-flex flex-column justify-content-center\"><img src=\"/images/brands_8.jpg\" alt=\"\"></div></div>

\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<!-- Brands Slider Navigation -->
\t\t\t\t\t\t<div class=\"brands_nav brands_prev\"><i class=\"fas fa-chevron-left\"></i></div>
\t\t\t\t\t\t<div class=\"brands_nav brands_next\"><i class=\"fas fa-chevron-right\"></i></div>

\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<!-- Newsletter -->

\t<div class=\"newsletter\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t\t<div class=\"newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center\">
\t\t\t\t\t\t<div class=\"newsletter_title_container\">
\t\t\t\t\t\t\t<div class=\"newsletter_icon\"><img src=\"/images/send.png\" alt=\"\"></div>
\t\t\t\t\t\t\t<div class=\"newsletter_title\">Sign up for Newsletter</div>
\t\t\t\t\t\t\t<div class=\"newsletter_text\"><p>...and receive %20 coupon for first shopping.</p></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"newsletter_content clearfix\">
\t\t\t\t\t\t\t<form action=\"#\" class=\"newsletter_form\">
\t\t\t\t\t\t\t\t<input type=\"email\" class=\"newsletter_input\" required=\"required\" placeholder=\"Enter your email address\">
\t\t\t\t\t\t\t\t<button class=\"newsletter_button\">Subscribe</button>
\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t<div class=\"newsletter_unsubscribe_link\"><a href=\"#\">unsubscribe</a></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<!-- Footer -->

\t<footer class=\"footer\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">

\t\t\t\t<div class=\"col-lg-3 footer_col\">
\t\t\t\t\t<div class=\"footer_column footer_contact\">
\t\t\t\t\t\t<div class=\"logo_container\">
\t\t\t\t\t\t\t<div class=\"logo\"><a href=\"#\">OneTech</a></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"footer_title\">Got Question? Call Us 24/7</div>
\t\t\t\t\t\t<div class=\"footer_phone\">+38 068 005 3570</div>
\t\t\t\t\t\t<div class=\"footer_contact_text\">
\t\t\t\t\t\t\t<p>17 Princess Road, London</p>
\t\t\t\t\t\t\t<p>Grester London NW18JR, UK</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"footer_social\">
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fab fa-facebook-f\"></i></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fab fa-twitter\"></i></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fab fa-youtube\"></i></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fab fa-google\"></i></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fab fa-vimeo-v\"></i></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-2 offset-lg-2\">
\t\t\t\t\t<div class=\"footer_column\">
\t\t\t\t\t\t<div class=\"footer_title\">Find it Fast</div>
\t\t\t\t\t\t<ul class=\"footer_list\">
\t\t\t\t\t\t\t<li><a href=\"#\">Computers & Laptops</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Cameras & Photos</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Hardware</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Smartphones & Tablets</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">TV & Audio</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t\t<div class=\"footer_subtitle\">Gadgets</div>
\t\t\t\t\t\t<ul class=\"footer_list\">
\t\t\t\t\t\t\t<li><a href=\"#\">Car Electronics</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-2\">
\t\t\t\t\t<div class=\"footer_column\">
\t\t\t\t\t\t<ul class=\"footer_list footer_list_2\">
\t\t\t\t\t\t\t<li><a href=\"#\">Video Games & Consoles</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Accessories</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Cameras & Photos</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Hardware</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Computers & Laptops</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"col-lg-2\">
\t\t\t\t\t<div class=\"footer_column\">
\t\t\t\t\t\t<div class=\"footer_title\">Customer Care</div>
\t\t\t\t\t\t<ul class=\"footer_list\">
\t\t\t\t\t\t\t<li><a href=\"#\">My Account</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Order Tracking</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Wish List</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Customer Services</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Returns / Exchange</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">FAQs</a></li>
\t\t\t\t\t\t\t<li><a href=\"#\">Product Support</a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t</div>
\t\t</div>
\t</footer>

\t<!-- Copyright -->

\t<div class=\"copyright\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start\">
\t\t\t\t\t\t<div class=\"copyright_content\"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> by <a href=\"https://colorlib.com\" target=\"_blank\">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>
\t\t\t\t\t\t<div class=\"logos ml-sm-auto\">
\t\t\t\t\t\t\t<ul class=\"logos_list\">
\t\t\t\t\t\t\t\t<li><a href=\"#\"><img src=\"/images/logos_1.png\" alt=\"\"></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><img src=\"/images/logos_2.png\" alt=\"\"></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><img src=\"/images/logos_3.png\" alt=\"\"></a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#\"><img src=\"/images/logos_4.png\" alt=\"\"></a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<script src=\"/js/jquery-3.3.1.min.js\"></script>
<script src=\"/styles/bootstrap4/popper.js\"></script>
<script src=\"/styles/bootstrap4/bootstrap.min.js\"></script>
<script src=\"/plugins/greensock/TweenMax.min.js\"></script>
<script src=\"/plugins/greensock/TimelineMax.min.js\"></script>
<script src=\"/plugins/scrollmagic/ScrollMagic.min.js\"></script>
<script src=\"/plugins/greensock/animation.gsap.min.js\"></script>
<script src=\"/plugins/greensock/ScrollToPlugin.min.js\"></script>
<script src=\"/plugins/OwlCarousel2-2.2.1/owl.carousel.js\"></script>
<script src=\"/plugins/easing/easing.js\"></script>
<script src=\"/plugins/Isotope/isotope.pkgd.min.js\"></script>
<script src=\"/plugins/jquery-ui-1.12.1.custom/jquery-ui.js\"></script>
<script src=\"/plugins/parallax-js-master/parallax.min.js\"></script>
<script src=\"/js/shop_custom.js\"></script>
</body>

</html>", "shop.html.twig", "C:\\xampp\\htdocs\\php\\day06eshop\\templates\\shop.html.twig");
    }
}
