<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_f582cecf3ba03ccd274661cb60f7600b7965154f844f05f6933c85c3adb88919 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "shop.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("shop.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "eShop - ";
        echo twig_escape_filter($this->env, ($context["pagetitle"] ?? null), "html", null, true);
    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"#email\").on('focusout keyup keypress blur change cut paste', function() {
                if(\$(\"#email\").val()!=\"\"){
                    \$.get(\"/checkemailavailable/\"+\$(\"#email\").val()).done(function(data){
                        if(data!=\"true\"){
                            \$(\"#isTaken\").html(\"This email has already been registered, please login.\");
                        }else{
                            \$(\"#isTaken\").html(\"\");
                        }
                    });
                }
            });
        });
        \$(document).ajaxError(function (event, jqxhr, settings, thrownError) {
            alert(\"Ajax error occured\");
        });
    </script>
    <link rel=\"stylesheet\" href=\"/styles/styling_form.css\" />
";
    }

    // line 27
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "    ";
        if (($context["errors"] ?? null)) {
            // line 29
            echo "        <ul class=\"errorMessage\">
            ";
            // line 30
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["err"]) {
                // line 31
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["err"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['err'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "        </ul>
    ";
        }
        // line 35
        echo "    <div class=\"form-style-10\">
        <h1>
            New User Registration
        </h1>
        <form id=\"signup\" method=\"post\">
            <div class=\"section\"><span>1</span>Name</div>
            <div class=\"inner-warp\">
                <label for=\"name\">Name</label>
                <input type=\"text\" id=\"name\" name=\"name\" maxlength=\"50\" required=\"required\" value=\"";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "name", [], "any", false, false, false, 43), "html", null, true);
        echo "\"/><br/>
            </div>
            <div class=\"section\"><span>2</span>Email</div>
            <div class=\"inner-warp\">
                <label for=\"email\">Email Address</label>
                <input type=\"email\" id=\"email\" name=\"email\" placeholder=\"yourname@xxx.com\" required=\"required\" value=\"";
        // line 48
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "email", [], "any", false, false, false, 48), "html", null, true);
        echo "\"/>
                <span id=\"isTaken\"></span>
            </div>
            <div class=\"section\"><span>3</span>Password</div>
            <div class=\"inner-warp\">
                <label for=\"password\">Password</label>
                <input type=\"password\" id=\"password\" name=\"password\" required=\"required\"/>
                <label for=\"confirm_password\">Confirm Password</label>
                <input type=\"password\" id=\"confirm_password\" name=\"confirm_password\" required=\"required\" />
            </div>
            <div class=\"button-section\">
                <input type=\"submit\" id=\"sign_up\" name=\"sign_up\" value=\"Sign Up\"/>
                <span class=\"privacy-policy\"><input type=\"checkbox\" id=\"isadmin\" name=\"isadmin\"/>Is admin</span>
            </div>
        </form>
    </div>
";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 48,  121 => 43,  111 => 35,  107 => 33,  98 => 31,  94 => 30,  91 => 29,  88 => 28,  84 => 27,  60 => 5,  56 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"shop.html.twig\" %}

{% block title %}eShop - {{pagetitle}}{% endblock %}
{% block head %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"#email\").on('focusout keyup keypress blur change cut paste', function() {
                if(\$(\"#email\").val()!=\"\"){
                    \$.get(\"/checkemailavailable/\"+\$(\"#email\").val()).done(function(data){
                        if(data!=\"true\"){
                            \$(\"#isTaken\").html(\"This email has already been registered, please login.\");
                        }else{
                            \$(\"#isTaken\").html(\"\");
                        }
                    });
                }
            });
        });
        \$(document).ajaxError(function (event, jqxhr, settings, thrownError) {
            alert(\"Ajax error occured\");
        });
    </script>
    <link rel=\"stylesheet\" href=\"/styles/styling_form.css\" />
{% endblock %}

{% block content %}
    {% if errors %}
        <ul class=\"errorMessage\">
            {% for err in errors %}
                <li>{{ err }}</li>
            {% endfor %}
        </ul>
    {% endif %}
    <div class=\"form-style-10\">
        <h1>
            New User Registration
        </h1>
        <form id=\"signup\" method=\"post\">
            <div class=\"section\"><span>1</span>Name</div>
            <div class=\"inner-warp\">
                <label for=\"name\">Name</label>
                <input type=\"text\" id=\"name\" name=\"name\" maxlength=\"50\" required=\"required\" value=\"{{ v.name }}\"/><br/>
            </div>
            <div class=\"section\"><span>2</span>Email</div>
            <div class=\"inner-warp\">
                <label for=\"email\">Email Address</label>
                <input type=\"email\" id=\"email\" name=\"email\" placeholder=\"yourname@xxx.com\" required=\"required\" value=\"{{ v.email }}\"/>
                <span id=\"isTaken\"></span>
            </div>
            <div class=\"section\"><span>3</span>Password</div>
            <div class=\"inner-warp\">
                <label for=\"password\">Password</label>
                <input type=\"password\" id=\"password\" name=\"password\" required=\"required\"/>
                <label for=\"confirm_password\">Confirm Password</label>
                <input type=\"password\" id=\"confirm_password\" name=\"confirm_password\" required=\"required\" />
            </div>
            <div class=\"button-section\">
                <input type=\"submit\" id=\"sign_up\" name=\"sign_up\" value=\"Sign Up\"/>
                <span class=\"privacy-policy\"><input type=\"checkbox\" id=\"isadmin\" name=\"isadmin\"/>Is admin</span>
            </div>
        </form>
    </div>
{% endblock %}", "register.html.twig", "C:\\xampp\\htdocs\\php\\day06eshop\\templates\\register.html.twig");
    }
}
