<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* passport.html.twig */
class __TwigTemplate_92f309bc8aa25a8570cb819dffbd80c4e2b836f39c6ec319517d57cd4d224687 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<form method=\"post\" enctype=\"multipart/form-data\">
    <!-- upload of a single file -->
    <p>
        <label>Add file (single): </label><br/>
        <input type=\"file\" name=\"example1\"/>
    </p>

    <!-- multiple input fields for the same input name, use brackets -->
    <p>
        <label>Add files (up to 2): </label><br/>
        <input type=\"file\" name=\"example2[]\"/><br/>
        <input type=\"file\" name=\"example2[]\"/>
    </p>

    <!-- one file input field that allows multiple files to be uploaded, use brackets -->
    <p>
        <label>Add files (multiple): </label><br/>
        <input type=\"file\" name=\"example3[]\" multiple=\"multiple\"/>
    </p>

    <p>
        <input type=\"submit\"/>
    </p>
</form>";
    }

    public function getTemplateName()
    {
        return "passport.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<form method=\"post\" enctype=\"multipart/form-data\">
    <!-- upload of a single file -->
    <p>
        <label>Add file (single): </label><br/>
        <input type=\"file\" name=\"example1\"/>
    </p>

    <!-- multiple input fields for the same input name, use brackets -->
    <p>
        <label>Add files (up to 2): </label><br/>
        <input type=\"file\" name=\"example2[]\"/><br/>
        <input type=\"file\" name=\"example2[]\"/>
    </p>

    <!-- one file input field that allows multiple files to be uploaded, use brackets -->
    <p>
        <label>Add files (multiple): </label><br/>
        <input type=\"file\" name=\"example3[]\" multiple=\"multiple\"/>
    </p>

    <p>
        <input type=\"submit\"/>
    </p>
</form>", "passport.html.twig", "C:\\xampp\\htdocs\\php\\day06eshop\\templates\\passport.html.twig");
    }
}
