<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>Document</title>
</head>
<body>
    <div id="centeredContent">
    <?php
    require_once "db.php";

        function displayForm($passportno=""){
            $form = <<<EOD
            <form method="post" enctype="multipart/form-data">
            passportno: <input type="text" name="passportno" value="$passportno"></br>
            photo file: <input type="file" name="image" /></br>
            <input type="submit" value="Add passport"></form>
EOD;

            echo $form;
        }

        if(isset($_POST['passportno'])&&isset($_FILES['image']))
        {
            $passportno = $_POST['passportno'];
            $errors= array();

            if(preg_match('/^[A-Z]{2}[0-9]{6}$/',$passportno)!=1){
                $errors[]='passportno must be in AB123456 fromat exactly.';
                $passportno = "";
            }
            $file_name = $_FILES['image']['name'];
            $file_size =$_FILES['image']['size'];
            $file_tmp =$_FILES['image']['tmp_name'];


            if($file_size > 2097152){
               $errors[]='File size must be 2 MB or smaller.';
            }
            $imginfo = getimagesize($file_tmp);
            if(!$imginfo){
                $errors[]="must upload image file.";
            }
            else{
                if ($imginfo[0] > 200 || $imginfo[1] > 1000)
                {
                    unlink($file_tmp);
                    $errors[]="Image's width and height must be in 200x1000 pixels.";
                }
            }
            switch ($imginfo['mine']){
                case 'image/gif':
                    $file_name = $passportno .".gif";
                    break;
                case 'image/png':
                    $file_name = $passportno .".png";
                    break;
                case 'image/jpg':
                    $file_name = $passportno .".jpg";
                    break;
                default:
                    $errors[]="Image's type must be jpg, png and gif.";
            }            

            $file = "uploads/".$file_name;
            if(empty($errors)==true){
                if (move_uploaded_file($file_tmp, $file)) {
                    echo db_addpassport($passportno,$file_name);
                }          
            }else{
                echo "<ul>";
                foreach($errors as $err)
                {
                    echo "<li>$err</li>";
                }
                echo "</ul>";
                displayForm($passportno);
            }
        }else{
            displayForm();
        }

    ?>
    </div>
</body>
</html>