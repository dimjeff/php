<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>Document</title>
</head>
<body>
    <div id="centeredContent">
<?php
        require_once 'db.php';
    function displayForm($title="",$content="",$articleid=""){
        $form = <<<MARKER
        <div class="divcenter"><h2>Create/Edit article</h2><div>
        <form method="post">
        Title: <input type="text" name="title" value="$title"></br>
        <input hidden id="articleid" name="articleid" value="$articleid">
        Content: <textarea name="content" rows="4" cols="50">$content</textarea></br>
        <input type="submit" value="Create/Edit">
    </form>
MARKER;
        echo $form;
    }

    if(!isset($_SESSION['user'])){            
        echo '<div class="divright"><a href="login.php">Login</a> or <a href="register.php">Register</a> to post articles or comments</div>';
        exit();
    }
    
    echo '<div class="divright">Yor are logged in as '. $_SESSION['user']['username'].' <a href="logout.php">Logout</a></div>';
    
    if(isset($_POST['title'])){
        $title = $_POST['title'];
        $content = $_POST['content'];
        $errors= array();
        if(strlen($title)<10||strlen($title)>100){
            $errors[]='title must be 10-100 chars. ';
        }
        if(strlen($content)<50||strlen($content)>4000){
            $errors[]='content must be 50-4000 chars. ';
        }
        if(empty($errors)==true){
            if(isset($_POST['articleid'])&&$_POST['articleid']!=""){
                $articleid = $_POST['articleid'];
                $sql = sprintf("UPDATE article set title='%s',body='%s' where id=%s and authorId=%s",
                mysqli_real_escape_string($conn,$title),
                mysqli_real_escape_string($conn,$content),
                mysqli_real_escape_string($conn,$articleid),
                mysqli_real_escape_string($conn,$_SESSION['user']['id'])
            );
            }else{
                $sql = sprintf("INSERT INTO article(title,body,authorId) values('%s','%s','%s')",
                mysqli_real_escape_string($conn,$title),
                mysqli_real_escape_string($conn,$content),
                mysqli_real_escape_string($conn,$_SESSION['user']['id']));
            }

            if(!mysqli_query($conn, $sql)){
                echo "Failed to execute MySQL query:" . mysqli_error($conn);
                exit();
            }
            echo "Successful, please go to homepage <a href='/'>CLick</a>.";
        }else{
            echo "<ul>";
            foreach($errors as $err)
            {
                echo "<li>$err</li>";
            }
            echo "</ul>";
            displayForm($title,$content);
        }        
    }else{
        if(isset($_GET['id'])){
            $articleid = $_GET['id'];
            $sql = sprintf("SELECT title,body FROM article where id='%s' and authorId=%s", mysqli_real_escape_string($conn,$articleid), mysqli_real_escape_string($conn,$_SESSION['user']['id']));
            $result = mysqli_query($conn,$sql); //$conn->query($sql);
            if(!$result){//SQL query failed.
                echo "SQL query failed";
            }else{
                if ($result->num_rows > 0) {
                    $row = $result->fetch_assoc();
                    displayForm($row["title"],$row["body"],$articleid);
                }else
                    header("Location: http://day02.ipd20");
            }
        }else
            displayForm();
    }
        
?>
    </div>
</body>
</html>

