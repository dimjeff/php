<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.html.twig */
class __TwigTemplate_a15ff99f39d6faf38ff906760e20d997c0e354a56c372b37fc4d00078806c060 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "My Blog - Login";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        if (($context["error"] ?? null)) {
            // line 7
            echo "        <ul class=\"errorMessage\">
            <li>";
            // line 8
            echo twig_escape_filter($this->env, ($context["error"] ?? null), "html", null, true);
            echo "</li>
        </ul>
    ";
        }
        // line 11
        echo "    <div class=\"form-style-10\">
        <form method=\"post\" id=\"signup\" >
            <div class=\"inner-warp\">
                <label for=\"username\">Username</label>
                <input type=\"text\" id=\"username\" name=\"username\" maxlength=\"50\" required=\"required\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "username", [], "any", false, false, false, 15), "html", null, true);
        echo "\"/><br/>
                <label for=\"password\">Password</label>
                <input type=\"password\" id=\"password\" name=\"password\" required=\"required\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "password", [], "any", false, false, false, 17), "html", null, true);
        echo "\"/>
            </div>
            <div class=\"divcenter\">
                <input type=\"submit\" id=\"login\" name=\"login\" value=\"Login\"/>
                <input type=\"button\" id=\"home\" name=\"home\" value=\"Home\" onclick=\"window.location.href = '/';\"/>
            </div>
        </form>
    </div>
";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 17,  76 => 15,  70 => 11,  64 => 8,  61 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}My Blog - Login{% endblock %}

{% block content %}
    {% if error %}
        <ul class=\"errorMessage\">
            <li>{{ error }}</li>
        </ul>
    {% endif %}
    <div class=\"form-style-10\">
        <form method=\"post\" id=\"signup\" >
            <div class=\"inner-warp\">
                <label for=\"username\">Username</label>
                <input type=\"text\" id=\"username\" name=\"username\" maxlength=\"50\" required=\"required\" value=\"{{ v.username }}\"/><br/>
                <label for=\"password\">Password</label>
                <input type=\"password\" id=\"password\" name=\"password\" required=\"required\" value=\"{{ v.password }}\"/>
            </div>
            <div class=\"divcenter\">
                <input type=\"submit\" id=\"login\" name=\"login\" value=\"Login\"/>
                <input type=\"button\" id=\"home\" name=\"home\" value=\"Home\" onclick=\"window.location.href = '/';\"/>
            </div>
        </form>
    </div>
{% endblock %}", "login.html.twig", "C:\\xampp\\htdocs\\php\\day04slimblog\\templates\\login.html.twig");
    }
}
