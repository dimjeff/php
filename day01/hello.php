<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <?php 

    function displayForm(){
        $form = <<<MARKER
        <form>
        Enter your name: <input type="text" name="name">
        <input type="submit" value="Say hello">
    </form>
MARKER;
        echo $form;
    }

        if(isset($_GET['name']))
        {
            $name = $_GET['name'];
            if(strlen($name)<2||strlen($name)>20){
                echo "Error: name must be 2-20 chars";
                displayForm();
            }else{
                echo "<p>Hello <b>" . $name . "</b>, nice to meet you!</p>";
            }            
        }else{
            displayForm();
        }
    ?>

</body>
</html>