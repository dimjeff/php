<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>Document</title>
</head>
<body>
    <div id="centeredContent">
    <?php
    require_once "db.php";

        function displayForm($name="",$gpa="",$isgrad="false",$gender="male"){
            $isgrad = $isgrad=='true'?"checked":"";
            $ismale = $gender=="male"? 'checked="true"':"";
            $form = <<<EOD
            <form method="post">
            Name: <input type="text" name="name" value="$name"></br>
            GPA: <input type="number" name="gpa" value="$gpa"></br>
            Is graduate: <input type="checkbox" name="isgrad" $isgrad><br/>
EOD;
            $form .= 'Gender:<input type="radio" id="male" name="gender" value="male"';
            if($gender=='male' || $gender=='')
                $form .= ' checked="true"';

            $form .= '>Male, <input type="radio" id="female" name="gender" value="female"';
            if($gender=='female')
                $form .= ' checked="true"';

            $form .= '>Female, <input type="radio" id="other" name="gender" value="other"';
            if($gender=='other')
                $form .= ' checked="true"';
            $form .= '">other <br/><input type="submit" value="Add person"></form>';

            echo $form;
        }

        if(isset($_POST['name'])&&isset($_POST['gpa']))
        {
            $name = $_POST['name'];
            $gpa = $_POST['gpa'];
            $isgrad = isset($_POST['isgrad'])?"true":"false";
            $gender = $_POST['gender'];
            $errList = array();
    
            if(strlen($name)<2||strlen($name)>20){
                array_push($errList,'name must be 2-20 chars');
            }

            if(empty($gpa) || !is_numeric($gpa) || $gpa < 0 || $gpa > 4.3){
                array_push($errList,'gpa must be 0-4.3');
            }
    
            if($errList){
                echo "<ul>";
                foreach($errList as $err)
                {
                    echo "<li>$err</li>";
                }
                echo "</ul>";
                displayForm($name,$gpa,$isgrad,$gender);
            }else{
/*                 $fp = fopen('people.txt', 'a');//opens file in append mode  
                fwrite($fp, $name.";".$age.PHP_EOL);  
                fclose($fp); */
                $person = new Person();
                $person->set_name($name);
                $person->set_gpa($gpa);
                $person->set_isgrad($isgrad);
                $person->set_gender($gender);

                echo db_insert($person);
            }
        }else{
            displayForm();
        }

    ?>
    </div>
</body>
</html>