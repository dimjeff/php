<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_f582cecf3ba03ccd274661cb60f7600b7965154f844f05f6933c85c3adb88919 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "My Blog - Registration";
    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"#username\").focusout(function(){
                if(\$(\"#username\").val()!=\"\"){
                    \$.get(\"/checkusernameavailable/\"+\$(\"#username\").val()).done(function(data){
                        if(data!=\"true\"){
                            //alert(\"This username has already been taken by another user.\");
                            \$(\"#isTaken\").html(\"This username has already been taken by another user.\");
                        }else{
                            \$(\"#isTaken\").html(\"\");
                        }
                    });
                }
            });
        });
        \$(document).ajaxError(function (event, jqxhr, settings, thrownError) {
            alert(\"Ajax error occured\");
        });
    </script>
";
    }

    // line 27
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "    ";
        if (($context["errors"] ?? null)) {
            // line 29
            echo "        <ul class=\"errorMessage\">
            ";
            // line 30
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["err"]) {
                // line 31
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["err"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['err'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "        </ul>
    ";
        }
        // line 35
        echo "    <div class=\"form-style-10\">
        <h1>
            New User Registration
        </h1>
        <form id=\"signup\" method=\"post\">
            <div class=\"section\"><span>1</span>Username</div>
            <div class=\"inner-warp\">
                <label for=\"username\">Username</label>
                <input type=\"text\" id=\"username\" name=\"username\" maxlength=\"50\" required=\"required\" value=\"";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "username", [], "any", false, false, false, 43), "html", null, true);
        echo "\"/><br/>
                <span id=\"isTaken\"></span>
            </div>

            <div class=\"section\"><span>2</span>Email</div>
            <div class=\"inner-warp\">
                <label for=\"email\">Email Address</label>
                <input type=\"email\" id=\"email\" name=\"email\" placeholder=\"yourname@xxx.com\" required=\"required\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "email", [], "any", false, false, false, 50), "html", null, true);
        echo "\"/>
            </div>
            <div class=\"section\"><span>3</span>Password</div>
            <div class=\"inner-warp\">
                <label for=\"password\">Password</label>
                <input type=\"password\" id=\"password\" name=\"password\" required=\"required\"/>
                <label for=\"confirm_password\">Confirm Password</label>
                <input type=\"password\" id=\"confirm_password\" name=\"confirm_password\" required=\"required\" />
            </div>
            <div class=\"divcenter\">
                <input type=\"submit\" id=\"sign_up\" name=\"sign_up\" value=\"Register!\"/>
            </div>
        </form>
    </div>
";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 50,  120 => 43,  110 => 35,  106 => 33,  97 => 31,  93 => 30,  90 => 29,  87 => 28,  83 => 27,  59 => 5,  55 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}My Blog - Registration{% endblock %}
{% block head %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"#username\").focusout(function(){
                if(\$(\"#username\").val()!=\"\"){
                    \$.get(\"/checkusernameavailable/\"+\$(\"#username\").val()).done(function(data){
                        if(data!=\"true\"){
                            //alert(\"This username has already been taken by another user.\");
                            \$(\"#isTaken\").html(\"This username has already been taken by another user.\");
                        }else{
                            \$(\"#isTaken\").html(\"\");
                        }
                    });
                }
            });
        });
        \$(document).ajaxError(function (event, jqxhr, settings, thrownError) {
            alert(\"Ajax error occured\");
        });
    </script>
{% endblock %}

{% block content %}
    {% if errors %}
        <ul class=\"errorMessage\">
            {% for err in errors %}
                <li>{{ err }}</li>
            {% endfor %}
        </ul>
    {% endif %}
    <div class=\"form-style-10\">
        <h1>
            New User Registration
        </h1>
        <form id=\"signup\" method=\"post\">
            <div class=\"section\"><span>1</span>Username</div>
            <div class=\"inner-warp\">
                <label for=\"username\">Username</label>
                <input type=\"text\" id=\"username\" name=\"username\" maxlength=\"50\" required=\"required\" value=\"{{ v.username }}\"/><br/>
                <span id=\"isTaken\"></span>
            </div>

            <div class=\"section\"><span>2</span>Email</div>
            <div class=\"inner-warp\">
                <label for=\"email\">Email Address</label>
                <input type=\"email\" id=\"email\" name=\"email\" placeholder=\"yourname@xxx.com\" required=\"required\" value=\"{{ v.email }}\"/>
            </div>
            <div class=\"section\"><span>3</span>Password</div>
            <div class=\"inner-warp\">
                <label for=\"password\">Password</label>
                <input type=\"password\" id=\"password\" name=\"password\" required=\"required\"/>
                <label for=\"confirm_password\">Confirm Password</label>
                <input type=\"password\" id=\"confirm_password\" name=\"confirm_password\" required=\"required\" />
            </div>
            <div class=\"divcenter\">
                <input type=\"submit\" id=\"sign_up\" name=\"sign_up\" value=\"Register!\"/>
            </div>
        </form>
    </div>
{% endblock %}", "register.html.twig", "C:\\xampp\\htdocs\\php\\day04slimblog\\templates\\register.html.twig");
    }
}
