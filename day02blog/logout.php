<?php

require_once 'db.php';

unset($_SESSION['user']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>You've been logged out. <a href="/">Click to continue</a></p>
</body>
</html>