<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require __DIR__ . '/vendor/autoload.php';

session_start();

$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'root';
DB::$password = '';
DB::$dbName = 'day06eshop';
DB::$port = 3306;
DB::$host = '127.0.0.1';
DB::$param_char = ':';
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    header("Location: /error_internal");
    global $log;
    $log->error("database error:". $params['error']);
    if(isset($params['query'])){
        $log->error("SQL query error:".$params['query']);
    };
    die;
  }

$app = AppFactory::create();

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ .'/cache', 'debug' => true]);
$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user'])?$_SESSION['user']:false);

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/error_forbidden', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_notfound', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_notfound.html.twig');
});

$app->get('/', function (Request $request, Response $response, array $args) use ($app) {
    $view = Twig::fromRequest($request);
    if(!isset($_SESSION['user'])){
        $user = array();
    }else{
        $user = $_SESSION['user'];
    }

    //$que = $request->getQueryParams();
    //$pag = $request->getParsedBody();

    $results_per_page = 4; // number of results per page
    $page =  1;
    $start_from = ($page-1) * $results_per_page;

    $results = DB::query("SELECT id, name, unitPrice,pictureFilePath FROM products ORDER BY id DESC LIMIT :i, :i", $start_from, $results_per_page);
    $records = DB::queryFirstField("SELECT COUNT(ID) AS total FROM products"); 
    $total_pages = ceil($records / $results_per_page);

    return $view->render($response, 'index.html.twig',[ 'pagetitle' => "All Products", 'products' => $results, 'totalpages' => $total_pages, 'currentpage'=>$page]);
});

$app->get('/checkemailavailable/[{email}]', function (Request $request, Response $response, array $args) {
    $email =  $args['email'];
    $results = DB::queryFirstColumn("SELECT id FROM users WHERE email=:s", $email);
    if($results){
        echo "false";
    }else{        
        echo "true";
    }

    return $response;
});

$app->get('/logout', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    unset($_SESSION['user']);
    return $view->render($response, 'index.html.twig');
});

$app->get('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'login.html.twig',[ 'pagetitle' => "Login"]);
});

$app->post('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();

    $email = $postvars['email'];
    $password = $postvars['password'];

    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=:s", $email);
    if(!$user){
        $error = "Invalid email or password, please check again.";
        unset($postvars['password']);
        return $view->render($response, 'login.html.twig', [
            'pagetitle' => "Login", 'v' => $postvars, 'error' => $error
        ]);
    }else{
        if($user['password']!=$password){
            $error = "Invalid email or password, please check again.";
            unset($postvars['password']);
            return $view->render($response, 'login.html.twig', [
                'pagetitle' => "Login", 'v' => $postvars, 'error' => $error
            ]);
        }
        unset($user['password']);
        $_SESSION['user']= $user;
        return $view->render($response, 'register_success.html.twig',[ 'pagetitle' => "Login Successful", 'OptationName' => "Login"]);
    }
});

$app->get('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig',[ 'pagetitle' => "Register New User"]);
});

$app->post('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();
    $errors= array();
    global $log;

    $name = $postvars['name'];
    $email = $postvars['email'];
    $password = $postvars['password'];
    $confirmpwd = $postvars['confirm_password'];
    $isAdmin = isset($postvars['isadmin'])?"true":"false";

    $result = file_get_contents("http://day06eshop/checkemailavailable/$email");
    if($result != "true"){
        $errors[]='This email has already been registered.';
        $postvars['email']="";
    }else{
        if(filter_var($email, FILTER_VALIDATE_EMAIL)==FALSE){
            $errors[]='Email does not look valid.';
            $postvars['email']="";
        }
    }

    if(preg_match('/^[a-zA-Z\s]{2,20}$/',$name)!=1){
        $errors[]='The name must be 2-20 chars, Only letters can be accepted as valid input!';
        $postvars['name']="";
    }

    if($password!=$confirmpwd){
        $errors[]='password is not same with confirm password.';
        $postvars['password']="";
        $postvars['confirm_password']="";
    }else{
        if(preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/',$password)!=1){
            $errors[]='Password must be at least 6 characters long and must contain at least one uppercase letter, one lower case letter, and one number or special character. It must not be longer than 10 characters.';
            $postvars['password']="";
            $postvars['confirm_password']="";
        }
    }

    if($errors){
        return $view->render($response, 'register.html.twig', [
            'v' => $postvars, 'errors' => $errors
        ]);
    }else{
        DB::insert('users',['name'=>$name, 'email' => $email, 'password' => $password, 'isAdmin' => $isAdmin]);
        $newId = DB::insertId();
        $log->debug(sprintf("User registered: id=%d, email=%s", $newId, $email));
        return $view->render($response, 'register_success.html.twig',[ 'pagetitle' => "Register Successful", 'OptationName' => "Register"]);
    }
});

$app->map(['GET', 'POST', 'PUT'],'/admin/categories/list', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if(!isset($_SESSION['user'])){
        $user = array();
    }else{
        $user = $_SESSION['user'];
    }

    if (!$user || $user['isAdmin']!="true") {
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
    if($request->getMethod()!="GET"){
        $PostVars = $request->getParsedBody();
        if(isset($PostVars['name'])){
            global $log;
            DB::insert('categories',['name'=>$PostVars['name']]);
            $newId = DB::insertId();
            $log->debug(sprintf("Category added: id=%d, name=%s", $newId, $PostVars['name']));
        }
    }
    $results = DB::query("SELECT id, name FROM categories ORDER BY id DESC");
    return $view->render($response, 'allcategories.html.twig',[ 'pagetitle' => "All Categories", 'categories' => $results]);
});

$app->map(['GET', 'POST', 'PUT'],'/testpostget', function (Request $request, Response $response, array $args) {
    //$view = Twig::fromRequest($request);
    if($request->getMethod()=="GET"){
        $allGetVars = $request->getQueryParams();
        foreach($allGetVars as $key => &$param){
        //GET parameters list
        }
        //Single GET parameter
        $getParam = $allGetVars['title'];
    }else{
        //POST or PUT
        $allPostPutVars = $request->getParsedBody();
        foreach($allPostPutVars as $key => $paramite){
        //POST or PUT parameters list
        }
        //Single POST/PUT parameter
        $postParam = $allPostPutVars['postParam'];
    }

    return $response;
    //return $view->render($response, 'articleaddedit.html.twig',['user'=>$user]);
});


$app->addErrorMiddleware(true, true, true);

$app->run();
?>