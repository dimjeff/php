<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require __DIR__ . '/vendor/autoload.php';

session_start();

$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));



if (strpos($_SERVER['HTTP_HOST'], "ipd20.com") !== false) {
    // hosting on ipd20.com
    DB::$user = 'cp4966_jianzhang';
    DB::$password = 'AnbCKLBxm2X42jRh';
    DB::$dbName = 'cp4966_jianzhang';
} else { // local computer
    DB::$user = 'root';
    DB::$password = '';
    DB::$dbName = 'day04slimblog';
}

//DB::$port = 3306;
//DB::$host = 'jianzhang.ipd20.com';
DB::$param_char = ':';
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    header("Location: /error_internal");
    global $log;
    $log->error("database error:". $params['error']);
    if(isset($params['query'])){
        $log->error("SQL query error:".$params['query']);
    };
    die;
    
    echo "Error: " . $params['error'] . "<br>\n";
    echo "Query: " . $params['query'] . "<br>\n";
    die; // don't want to keep going if a query broke
  }

$app = AppFactory::create();

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ .'/cache', 'debug' => true]);
$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user'])?$_SESSION['user']:false);

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

$app->get('/problem', function (Request $request, Response $response, array $args) {
    DB::query("select * from dfsd");
    return $response;
});

$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    echo "internal error, cannot continue....</br>please to <a href='/'>Home</a>.";
    return $response;
});

$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if(!isset($_SESSION['user'])){
        $user = array();
    }else{
        $user = $_SESSION['user'];
    }

    $results_per_page = 5; // number of results per page
    $page =  1;
    $start_from = ($page-1) * $results_per_page;

    $results = DB::query("SELECT a.id,a.title,a.body,DATE_FORMAT(a.creationTime, '%b %e, %Y') as postdate,DATE_FORMAT(a.creationTime, '%H:%i') as posttime,u.username as author FROM article a inner join user u on a.authorId=u.id ORDER BY a.id DESC LIMIT :i, :i", $start_from, $results_per_page);
    $records = DB::queryFirstField("SELECT COUNT(ID) AS total FROM article"); 
    $total_pages = ceil($records / $results_per_page);
    foreach($results as &$article){
        $article['body'] = strip_tags($article['body']);
        $article['body'] = substr($article['body'],0,150) . (strlen($article['body'])>150?"......":"");
    }
    return $view->render($response, 'index.html.twig',['user' => $user, 'articles' => $results, 'totalpages' => $total_pages, 'currentpage'=>$page]);
});

$app->get('/index/{page:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if(!isset($_SESSION['user'])){
        $user = array();
    }else{
        $user = $_SESSION['user'];
    }

    $page =  $args['page'];
    $results_per_page = 5; // number of results per page
    $start_from = ($page-1) * $results_per_page;

    $results = DB::query("SELECT a.id,a.title,a.body,DATE_FORMAT(a.creationTime, '%b %e, %Y') as postdate,DATE_FORMAT(a.creationTime, '%H:%i') as posttime,u.username as author FROM article a inner join user u on a.authorId=u.id ORDER BY a.id DESC LIMIT :i, :i", $start_from, $results_per_page);
    $records = DB::queryFirstField("SELECT COUNT(ID) AS total FROM article"); 
    $total_pages = ceil($records / $results_per_page);
    foreach($results as &$article){
        $article['body'] = strip_tags($article['body']);
        $article['body'] = substr($article['body'],0,150) .  (strlen($article['body'])>150?"......":"");
    }
    return $view->render($response, 'index.html.twig',['user' => $user, 'articles' => $results, 'totalpages' => $total_pages, 'currentpage'=>$page]);
});

$app->get('/article/{articleid:[0-9]+}[/{page:[0-9]+}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $articleid = $args['articleid'];
    $page = 1;
    if(isset($args['page']))
        $page = $args['page'];
    if(!isset($_SESSION['user'])){
        $user = array();
    }else{
        $user = $_SESSION['user'];
    }
    $results_per_page = 5; // number of results per page
    $start_from = ($page-1) * $results_per_page;

    //to handle if article is null
    $article = DB::queryFirstRow("SELECT a.id,a.title,a.body,DATE_FORMAT(a.creationTime, '%b %e, %Y') as postdate,DATE_FORMAT(a.creationTime, '%H:%i') as posttime,u.username as author FROM article a inner join user u on a.authorId=u.id WHERE a.id=:i", $articleid);
    $results = DB::query("SELECT u.username as author,DATE_FORMAT(c.creationTime, '%b %e, %Y') as postdate,DATE_FORMAT(c.creationTime, '%H:%i') as posttime,c.body FROM comment c inner join user u on c.authorId = u.id where c.articleId=:i ORDER BY c.id DESC LIMIT :i, :i",$articleid , $start_from, $results_per_page);

    $records = DB::queryFirstField("SELECT COUNT(ID) AS total FROM comment WHERE articleId=:i",$articleid); 
    $total_pages = ceil($records / $results_per_page);

    return $view->render($response, 'article.html.twig',[ 'user' => $user, 'article' => $article, 
    'comments' => $results, 'totalpages' => $total_pages, 'currentpage'=>$page ]);
});

$app->post('/article/{articleid:[0-9]+}[/{page:[0-9]+}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $articleid = $args['articleid'];
    $page = 1;
    if(isset($args['page']))
        $page = $args['page'];
    if(!isset($_SESSION['user'])){
        $user = array();
        echo "please login first.";
        return $response;
    }else{
        $user = $_SESSION['user'];
    }
    $error = "";
    $postvars = $request->getParsedBody();
    $comment = $postvars['comment'];
    if(strlen($comment)<5||strlen($comment)>5000){
        $error='comment must be 5-5000 chars. ';
    }

    if(!$error){
        DB::insert('comment',['articleid'=>$articleid, 'authorId' => $user['id'], 'body' => $comment]);
    }

    $results_per_page = 5; // number of results per page
    $start_from = ($page-1) * $results_per_page;

    //to handle if article is null
    $article = DB::queryFirstRow("SELECT a.id,a.title,a.body,DATE_FORMAT(a.creationTime, '%b %e, %Y') as postdate,DATE_FORMAT(a.creationTime, '%H:%i') as posttime,u.username as author FROM article a inner join user u on a.authorId=u.id WHERE a.id=:i", $articleid);
    $results = DB::query("SELECT u.username as author,DATE_FORMAT(c.creationTime, '%b %e, %Y') as postdate,DATE_FORMAT(c.creationTime, '%H:%i') as posttime,c.body FROM comment c inner join user u on c.authorId = u.id where c.articleId=:i ORDER BY c.id DESC LIMIT :i, :i",$articleid , $start_from, $results_per_page);
    $records = DB::queryFirstField("SELECT COUNT(ID) AS total FROM comment WHERE articleId=:i",$articleid); 
    $total_pages = ceil($records / $results_per_page);

    return $view->render($response, 'article.html.twig',[ 'user' => $user, 'article' => $article, 'comments' => $results, 
    'totalpages' => $total_pages, 'currentpage'=>$page, 'error'=>$error, 'comment'=>$comment ]);
});


$app->get('/checkusernameavailable/{username}', function (Request $request, Response $response, array $args) {
    $username =  $args['username'];
    $results = DB::queryFirstRow("SELECT * FROM user WHERE username=:s", $username);
    if($results){
        echo "false";
    }else{        
        echo "true";
    }

    return $response;
});

$app->get('/logout', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    unset($_SESSION['user']);
    return $view->render($response, 'index.html.twig');
});

$app->get('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'login.html.twig');
});

$app->post('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();

    $username = $postvars['username'];
    $password = $postvars['password'];

    $user = DB::queryFirstRow("SELECT * FROM user WHERE username=:s", $username);
    if(!$user){
        if($user['password']!=$password){
            $error = "Invalid username or password, please check again.";
            return $view->render($response, 'login.html.twig', [
                'v' => $postvars, 'error' => $error
            ]);
        }
    }else{    
        unset($user['password']);
        $_SESSION['user']= $user;
        echo "Login successful, please to <a href='/'>Home</a>.";
        return $response;
    }
});

$app->get('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig');
});

$app->post('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();
    $errors= array();
    global $log;

    $username = $postvars['username'];
    $email = $postvars['email'];
    $password = $postvars['password'];
    $confirmpwd = $postvars['confirm_password'];

    $result = file_get_contents("http://day04slimblog/checkusernameavailable/$username");
    if($result != "true"){
        $errors[]='This username has already been taken by another user.';
        $postvars['username']="";
    }else{
        if(preg_match('/^[a-z0-9]{4,20}$/',$username)!=1){
            $errors[]='username must be 4-20 chars. including lower case letters and numbers';
            $postvars['username']="";
        }
    }

    if(filter_var($email, FILTER_VALIDATE_EMAIL)==FALSE){
        $errors[]='Email does not look valid.';
        $postvars['email']="";
    }

    if($password!=$confirmpwd){
        $errors[]='password is not same with confirm password.';
        $postvars['password']="";
    }else{
        if(preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/',$password)!=1){
            $errors[]='Password must be at least 6 characters long and must contain at least one uppercase letter, one lower case letter, and one number or special character. It must not be longer than 10 characters.';
            $postvars['password']="";
            $postvars['confirm_password']="";
        }
    }

    if($errors){
        return $view->render($response, 'register.html.twig', [
            'v' => $postvars, 'errors' => $errors
        ]);
    }else{
        DB::insert('user',['username'=>$username, 'email' => $email, 'password' => $password]);
        $newId = DB::insertId();
        $log->debug(sprintf("User registered: id=%d, username=%s", $newId, $username));
        $response->getBody()->write("Successful, please to <a href='/login'>login</a>.");
        return $response; 
    }
});

$app->get('/articleaddedit[/{id:[0-9]+}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    if(!isset($_SESSION['user'])){
        $user = array();
        echo "Please login first. </br><a href='/login'>click to login</a>";
        return $response;
    }else{
        $user = $_SESSION['user'];
    }

    if($args){
        $results = DB::queryFirstRow("select * from article where id=:i and authorId=:i", $args['id'], $user['id']);
        if(!$results){
            echo "You cannot edit other person's article.</br><a href='/'>home</a>";
            return $response;
        }
        return $view->render($response, 'articleaddedit.html.twig',['user'=>$user, 'article' => $results]);
    }else{
        return $view->render($response, 'articleaddedit.html.twig',['user'=>$user]);
    }
});

$app->post('/articleaddedit[/{id:[0-9]+}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();

    $title = $postvars['title'];
    $content = $postvars['content'];
    // FIXME: sanitize body - 1) only allow certain HTML tags, 2) make sure it is valid html
    // WARNING: If you forget to sanitize the body bad things may happen such as JavaScript injection
    $content = strip_tags($content, "<p><ul><li><em><strong><i><b><ol><h3><h4><span>");

    $errors= array();

    if(!isset($_SESSION['user'])){
        $user = array();//set user is null
        echo "Please login first. </br><a href='/login'>click to login</a>";
        return $response;
    }else{
        $user = $_SESSION['user'];
    }

    if(strlen($title)<10||strlen($title)>100){
        $errors[]='title must be 10-100 chars. ';
    }
    if(strlen($content)<50||strlen($content)>4000){
        $errors[]='content must be 50-4000 chars. ';
    }
 
    if($errors){
        return $view->render($response, 'articleaddedit.html.twig', [
            'v' => $postvars, 'error' => $errors
        ]);
    }else{
        if($args){
            DB::query("UPDATE article SET title=:s, body=:s WHERE id=:i AND authorId=:i", $title, $content, $args['id'], $user['id']);
            echo "Successful, please back to <a href='/article/". $args['id'] ."/1'>Article</a>.";
        }else{
            DB::insert('article',['authorId'=> $user['id'], 'title' => $title, 'body' => $content]);
            echo "Successful, please to <a href='/'>Home</a>.";
        }
    }
    return $response;
});


$app->map(['GET', 'POST', 'PUT'],'/testpostget', function (Request $request, Response $response, array $args) {
    //$view = Twig::fromRequest($request);
    if($request->getMethod()=="GET"){
        $allGetVars = $request->getQueryParams();
        foreach($allGetVars as $key => &$param){
        //GET parameters list
        }
        //Single GET parameter
        $getParam = $allGetVars['title'];
    }else{
        //POST or PUT
        $allPostPutVars = $request->getParsedBody();
        foreach($allPostPutVars as $key => $paramite){
        //POST or PUT parameters list
        }
        //Single POST/PUT parameter
        $postParam = $allPostPutVars['postParam'];
    }

    return $response;
    //return $view->render($response, 'articleaddedit.html.twig',['user'=>$user]);
});
/* 
$app->post('/articleaddedit', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();

    $title = $postvars['title'];
    $content = $postvars['content'];
    $articleid = $postvars['articleid'];
    $errors= array();

    if(strlen($title)<10||strlen($title)>100){
        $errors[]='title must be 10-100 chars. ';
    }
    if(strlen($content)<50||strlen($content)>4000){
        $errors[]='content must be 50-4000 chars. ';
    }
 
    if($errors){
        return $view->render($response, 'articleaddedit.html.twig', [
            'v' => $postvars, 'error' => $errors, 'articleid' => $articleid
        ]);
    }else{
        DB::insert('article',['authorId'=>$_SESSION['user']['id'], 'title' => $title, 'body' => $content]);
        echo "Successful, please to <a href='/'>Home</a>.";
    }
    return $response;
}); */

$app->addErrorMiddleware(true, true, true);

$app->run();
?>