<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_b5e37d6f3d875171550d3f1e2c3419a20e84a59615b65f0aef15fb5e848fa1fa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>        
        <link rel=\"stylesheet\" href=\"/styles.css\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo " - Quiz1SlimShout</title>
        ";
        // line 6
        $this->displayBlock('head', $context, $blocks);
        // line 7
        echo "    </head>
    <body>
        <div id=\"centeredContent\">
            ";
        // line 10
        if (($context["userSession"] ?? null)) {
            // line 11
            echo "                <div class=\"divright\">Yor are logged in as ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["userSession"] ?? null), "username", [], "any", false, false, false, 11), "html", null, true);
            echo "</div>
                <div class=\"divright\">You can <a href=\"/logout\">logout</a> or <a href=\"/shouts/add\">send a message</a></div>
            ";
        } else {
            // line 14
            echo "                <div class=\"divright\">you can <a href=\"/login\">Login</a> or <a href=\"/register\">Register</a> to send messages</div>
            ";
        }
        // line 16
        echo "            <div id=\"content\">";
        $this->displayBlock('content', $context, $blocks);
        echo "</div>
            <div id=\"footer\" class=\"divright\">
                &copy; Copyright 2020 by <a href=\"http://quiz1slimshout/\"> JIAN</a>.
            </div>
        </div>
    </body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Default";
    }

    // line 6
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 16
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 16,  89 => 6,  82 => 5,  70 => 16,  66 => 14,  59 => 11,  57 => 10,  52 => 7,  50 => 6,  46 => 5,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>        
        <link rel=\"stylesheet\" href=\"/styles.css\" />
        <title>{% block title %}Default{% endblock %} - Quiz1SlimShout</title>
        {% block head %}{% endblock %}
    </head>
    <body>
        <div id=\"centeredContent\">
            {% if userSession %}
                <div class=\"divright\">Yor are logged in as {{ userSession.username }}</div>
                <div class=\"divright\">You can <a href=\"/logout\">logout</a> or <a href=\"/shouts/add\">send a message</a></div>
            {% else %}
                <div class=\"divright\">you can <a href=\"/login\">Login</a> or <a href=\"/register\">Register</a> to send messages</div>
            {% endif %}
            <div id=\"content\">{% block content %}{% endblock %}</div>
            <div id=\"footer\" class=\"divright\">
                &copy; Copyright 2020 by <a href=\"http://quiz1slimshout/\"> JIAN</a>.
            </div>
        </div>
    </body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\php\\quiz1slimshout\\templates\\master.html.twig");
    }
}
