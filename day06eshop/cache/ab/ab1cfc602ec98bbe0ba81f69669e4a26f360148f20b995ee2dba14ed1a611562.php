<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.html.twig */
class __TwigTemplate_a15ff99f39d6faf38ff906760e20d997c0e354a56c372b37fc4d00078806c060 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "shop.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("shop.html.twig", "login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "eShop - ";
        echo twig_escape_filter($this->env, ($context["pagetitle"] ?? null), "html", null, true);
    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    <link rel=\"stylesheet\" href=\"/styles/styling_form.css\" />
";
    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        if (($context["error"] ?? null)) {
            // line 9
            echo "        <ul class=\"errorMessage\">
            <li>";
            // line 10
            echo twig_escape_filter($this->env, ($context["error"] ?? null), "html", null, true);
            echo "</li>
        </ul>
    ";
        }
        // line 13
        echo "    <div class=\"form-style-10\">
        <form method=\"post\" id=\"signup\" >
            <div class=\"inner-warp\">
                <label for=\"email\">Email</label>
                <input type=\"text\" id=\"email\" name=\"email\" maxlength=\"50\" required=\"required\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "email", [], "any", false, false, false, 17), "html", null, true);
        echo "\"/><br/>
                <label for=\"password\">Password</label>
                <input type=\"password\" id=\"password\" name=\"password\" required=\"required\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "password", [], "any", false, false, false, 19), "html", null, true);
        echo "\"/>
            </div>
            <div class=\"divcenter\">
                <input type=\"submit\" id=\"login\" name=\"login\" value=\"Login\"/>
            </div>
        </form>
    </div>
";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 19,  87 => 17,  81 => 13,  75 => 10,  72 => 9,  69 => 8,  65 => 7,  60 => 5,  56 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"shop.html.twig\" %}

{% block title %}eShop - {{pagetitle}}{% endblock %}
{% block head %}
    <link rel=\"stylesheet\" href=\"/styles/styling_form.css\" />
{% endblock %}
{% block content %}
    {% if error %}
        <ul class=\"errorMessage\">
            <li>{{ error }}</li>
        </ul>
    {% endif %}
    <div class=\"form-style-10\">
        <form method=\"post\" id=\"signup\" >
            <div class=\"inner-warp\">
                <label for=\"email\">Email</label>
                <input type=\"text\" id=\"email\" name=\"email\" maxlength=\"50\" required=\"required\" value=\"{{ v.email }}\"/><br/>
                <label for=\"password\">Password</label>
                <input type=\"password\" id=\"password\" name=\"password\" required=\"required\" value=\"{{ v.password }}\"/>
            </div>
            <div class=\"divcenter\">
                <input type=\"submit\" id=\"login\" name=\"login\" value=\"Login\"/>
            </div>
        </form>
    </div>
{% endblock %}", "login.html.twig", "C:\\xampp\\htdocs\\php\\day06eshop\\templates\\login.html.twig");
    }
}
