<?php

use Slim\Factory\AppFactory;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require __DIR__ . '/vendor/autoload.php';

$app = AppFactory::create();

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

if (strpos($_SERVER['HTTP_HOST'], "ipd20.com") !== false) {
    // hosting on ipd20.com
    DB::$user = 'cp4966_teacher';
    DB::$password = 'UzXoLgOfibQ1Nk7n';
    DB::$dbName = 'cp4966_teacher';
    DB::$encoding = 'utf8'; // defaults to latin1 if omitted
} else { // local computer
    DB::$user = 'root';
    DB::$password = '';
    DB::$dbName = 'day15todorest';
    DB::$encoding = 'utf8'; // defaults to latin1 if omitted
}

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params)
{
    http_response_code(500);
    header('Content-Type: application/json');
    echo json_encode("Database error");
    global $log;
    $log->error("Database erorr[Connection]: " . $params['error']);
    if ($params['query']) {
        $log->error("Database error[Query]: " . $params['query']);
    }
    die();
}

// Add Error Middleware for 404 - not found handling
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$errorMiddleware->setErrorHandler(
    \Slim\Exception\HttpNotFoundException::class, 
        function () use ($app) {
            $response = $app->getResponseFactory()->createResponse();
            $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
            $response = $response->withStatus(404);
            $response->getBody()->write(json_encode("404 - not found"));
            return $response;
        }
);

$app->get('/', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Todo app with RESTful API");
    return $response;
});

$app->get('/api/todos', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $get = $request->getQueryParams();
    $sortBy=null;
    if(isset($get['sortBy'])){
        $sortBy = $get['sortBy'];
    }else{
        $sortBy = "id";
    }
    global $log;
    $userId = getAuthUserId($request->getHeaders());
    if (!$userId) {
        $response = $response->withStatus(403); // FIXME: should really be 401 instead, but not for JS
        $log->debug("GET /todos refused 403 - user/pass invalid");
        $response->getBody()->write(json_encode("403 - authentication failed"));
        return $response;
    }

    $todoList = DB::query("SELECT id, task, dueDate, isDone FROM todos WHERE ownerId=%s ORDER BY %b ASC", $userId, $sortBy);
    $log->debug($sortBy);
    $json = json_encode($todoList, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

$app->get('/api/todos/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $userId = getAuthUserId($request->getHeaders());
    if (!$userId) {
        $response = $response->withStatus(403); // FIXME: should really be 401 instead, but not for JS
        $log->debug("GET /todos/{id} refused 403 - user/pass invalid");
        $response->getBody()->write(json_encode("403 - authentication failed"));
        return $response;
    }
    $id = $args['id'];
    $todo = DB::queryFirstRow("SELECT id, task, dueDate, isDone FROM todos WHERE id=%s AND ownerId=%s", $id, $userId);
    if (!$todo) { // not found
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    }
    $json = json_encode($todo, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

$app->post('/api/register', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $json = $request->getBody();
    $user = json_decode($json, true); // true makes it return an associative array instead of an object
    // validate todo fields, 400 in case of error
    if ( ($result = validateUser($user)) !== TRUE) {
        global $log;
        $log->debug("POST /register failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    $users = DB::queryFirstRow("select * from users where email=%s",$user['email']);
    DB::insert('users', $user);
    $id = DB::insertId();
    $response = $response->withStatus(201); // record created
    $response->getBody()->write(json_encode($id));
    return $response;
});

$app->post('/api/todos', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $userId = getAuthUserId($request->getHeaders());
    if (!$userId) {
        $response = $response->withStatus(403); // FIXME: should really be 401 instead, but not for JS
        $log->debug("POST /todos refused 403 - user/pass invalid");
        $response->getBody()->write(json_encode("403 - authentication failed"));
        return $response;
    }
    $json = $request->getBody();
    $todo = json_decode($json, true); // true makes it return an associative array instead of an object
    // validate todo fields, 400 in case of error
    if ( ($result = validateTodo($todo)) !== TRUE) {
        global $log;
        $log->debug("POST /todos failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    $todo['ownerId'] = $userId;
    DB::insert('todos', $todo);
    $id = DB::insertId();
    $response = $response->withStatus(201); // record created
    $response->getBody()->write(json_encode($id));
    return $response;
});

$app->map(['PUT', 'PATCH'], '/api/todos/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $userId = getAuthUserId($request->getHeaders());
    if (!$userId) {
        $response = $response->withStatus(403); // FIXME: should really be 401 instead, but not for JS
        $log->debug("POST /todos refused 403 - user/pass invalid");
        $response->getBody()->write(json_encode("403 - authentication failed"));
        return $response;
    }
    $id = $args['id'];
    $json = $request->getBody();
    $todo = json_decode($json, true); // true makes it return an associative array instead of an object
    $method = $request->getMethod();
    // validate todo fields, 400 in case of error
    if ( ($result = validateTodo($todo, $method == 'PATCH')) !== TRUE) {
        global $log;
        $log->debug($method . " /todos failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    if (!DB::query("SELECT id FROM todos WHERE id=%s AND ownerId=%s", $id, $userId)) {
        global $log;
        $result = "Record with id $id does not exist";
        $log->debug($method . " /todos failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - " . $result));
        return $response;
    }
    DB::update('todos', $todo, "id=%s AND ownerId=%s", $id, $userId);
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses
    return $response;
});

$app->delete('/api/todos/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $userId = getAuthUserId($request->getHeaders());
    if (!$userId) {
        $response = $response->withStatus(403); // FIXME: should really be 401 instead, but not for JS
        $log->debug("POST /todos refused 403 - user/pass invalid");
        $response->getBody()->write(json_encode("403 - authentication failed"));
        return $response;
    }
    $id = $args['id'];
    DB::delete('todos', 'id=%s AND ownerId=%s', $id, $userId);
    $count = DB::affectedRows();
    $json = json_encode($count != 0, JSON_PRETTY_PRINT); // returns true/false
    $response->getBody()->write($json);
    return $response;
});

function validateUser($user) {
    global $log;
    if ($user === NULL) { // if json_decode fails it returns null - handle it here
        return "Invalid JSON data provided";
    }
    // does it have all the fields required and only the fields requried?
    $expectedFields = ['email', 'password'];
    $todoFields = array_keys($user);
    // check for fields that should not be there
    if (($diff = array_diff($todoFields, $expectedFields))) {
        return "Invalid fields in Todo: [" . implode(',',$diff) . "]";
    }
    // do not allow any field to be null (for both put and patch)
    foreach ($user as $key => $value) {
        if (@is_null($value)) {
            return "$key must not be null";
        }
    }
    // validate each field
    if (isset($user['email'])) { // in patch it may be absent
        if (is_null($user['email'])) {
            return "email must not be null";
        }
        $email = $user['email'];
        if (strlen($email) < 1 || strlen($email) > 20) {
            return "email must be 1-20 characters long";
        }
        if(filter_var($email, FILTER_VALIDATE_EMAIL)==FALSE){
            return 'Email does not look valid.';
        }
    }
    if (isset($user['password'])) {
        $password = $user['password'];
        if (strlen($password) < 1 || strlen($password) > 20) {
            return "password must be 1-20 characters long";
        }
    }
    $users = DB::queryFirstRow("select * from users where email=%s",$user['email']);
    if($users){
        return "user is exists";
    }
    return TRUE;
}

// return TRUE if all is fine otherwise return string describing the problem
function validateTodo($todo, $forPatch = false) {
    global $log;
    if ($todo === NULL) { // if json_decode fails it returns null - handle it here
        return "Invalid JSON data provided";
    }
    // does it have all the fields required and only the fields requried?
    $expectedFields = ['task', 'dueDate', 'isDone'];
    $todoFields = array_keys($todo);
    // check for fields that should not be there
    if (($diff = array_diff($todoFields, $expectedFields))) {
        return "Invalid fields in Todo: [" . implode(',',$diff) . "]";
    }
    if (!$forPatch) {
    // check for fields that are missing
        if (($diff = array_diff($expectedFields, $todoFields))) {
            return "Missing fields in Todo: [" . implode(',',$diff) . "]";
        }
    }
    // do not allow any field to be null (for both put and patch)
    foreach ($todo as $key => $value) {
        if (@is_null($value)) {
            return "$key must not be null";
        }
    }
    // validate each field
    if (isset($todo['task'])) { // in patch it may be absent
        if (is_null($todo['task'])) {
            return "Task description must not be null";
        }
        $task = $todo['task'];
        if (strlen($task) < 1 || strlen($task) > 100) {
            return "Task description must be 1-100 characters long";
        }
    }
    if (isset($todo['dueDate'])) {
        if (!date_create_from_format('Y-m-d', $todo['dueDate'])) {
            return "DueDate has invalid format";
        }
        $dueDate = strtotime($todo['dueDate']);
        if ($dueDate < strtotime('1900-01-01') || $dueDate > strtotime('2100-01-01')) {
            return "DueDate must be within 1900 to 2099 years";
        }
    }
    if (isset($todo['isDone'])) {
        if (!in_array($todo['isDone'], ['pending', 'done'])) {
            return "IsDone invalid: must be pending or done";
        }
    }
    //
    return TRUE;
}

// returns FALSE if authentication failed or missing
// returns userId if successful
function getAuthUserId($headersArray) {
    if (!isset($headersArray['X-auth-username'])) return FALSE;
    if (!isset($headersArray['X-auth-password'])) return FALSE;
    $username = $headersArray['X-auth-username'][0];
    $password = $headersArray['X-auth-password'][0];
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $username);
    $userId = FALSE;
    if ($user) {
        if ($user['password'] == $password) {
            $userId = $user['id'];
        }
    }
    return $userId;
}

$app->run();

