<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_e989d30591416b76ed4979b3d2a20be296a126417f9db421860e5fb9a72a72ea extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Welcome My Blog - Home";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <div class=\"divcenter\"><h1>Welcome to my blog</h1></div>
";
        // line 7
        if (($context["articles"] ?? null)) {
            // line 8
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["articles"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
                // line 9
                echo "        </br>
        <div> <h2><a href=\"/article/";
                // line 10
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "id", [], "any", false, false, false, 10), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "title", [], "any", false, false, false, 10), "html", null, true);
                echo "</a></h2></div>
        <div> <h3>Posted by ";
                // line 11
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "author", [], "any", false, false, false, 11), "html", null, true);
                echo " on ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "postdate", [], "any", false, false, false, 11), "html", null, true);
                echo " at ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "posttime", [], "any", false, false, false, 11), "html", null, true);
                echo "</h3></div>
        <div> <h4> ";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["article"], "body", [], "any", false, false, false, 12), "html", null, true);
                echo " </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 15
        if (($context["totalpages"] ?? null)) {
            // line 16
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, ($context["totalpages"] ?? null)));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 17
                echo "        <a href=\"/index/";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "\" ";
                if (0 === twig_compare($context["page"], ($context["currentpage"] ?? null))) {
                    echo "class=\"errorMessage";
                }
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</a>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 17,  96 => 16,  94 => 15,  85 => 12,  77 => 11,  71 => 10,  68 => 9,  63 => 8,  61 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Welcome My Blog - Home{% endblock %}

{% block content %}
    <div class=\"divcenter\"><h1>Welcome to my blog</h1></div>
{% if articles %}
    {% for article in articles %}
        </br>
        <div> <h2><a href=\"/article/{{ article.id }}\">{{ article.title }}</a></h2></div>
        <div> <h3>Posted by {{ article.author }} on {{ article.postdate }} at {{ article.posttime }}</h3></div>
        <div> <h4> {{ article.body }} </div>
    {% endfor %}
{% endif %}
{% if totalpages %}
    {% for page in 1..totalpages %}
        <a href=\"/index/{{ page }}\" {% if page == currentpage%}class=\"errorMessage{% endif %}\">{{ page }}</a>
    {% endfor %}
{% endif %}
{% endblock %}", "index.html.twig", "C:\\xampp\\htdocs\\php\\day04slimblog\\templates\\index.html.twig");
    }
}
