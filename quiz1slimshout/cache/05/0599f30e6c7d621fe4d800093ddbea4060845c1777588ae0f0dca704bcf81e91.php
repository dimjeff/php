<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* shoutslist.html.twig */
class __TwigTemplate_35cdd2d64222c60762e2061c292bf49651514e84676296489e0fbacd2732157a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "shoutslist.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Shouts List";
    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"#users\").on('change', function() {
                //\$(\"#users option:selected\" ).text()
                if(\$(\"#users option:selected\" ).text()!=\"\"){
                    \$.get(\"/ajax/loadshouts/by/\"+\$(\"#users option:selected\" ).text()).done(function(data){
                        \$(\"#shoutslist\").html(data);
                    });
                }else{
                    \$.get(\"/ajax/loadshouts\").done(function(data){
                        \$(\"#shoutslist\").html(data);
                    });
                }
            });
        });
        \$(document).ajaxError(function (event, jqxhr, settings, thrownError) {
            alert(\"Ajax error occured\");
        });
    </script>
";
    }

    // line 26
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "    <div class=\"divcenter\"><h1>Welcome to Shouts</h1></div>
    <div class=\"divcenter\">        
        <select id=\"users\" name=\"users\">
            <option value=\"\"></option>
            ";
        // line 31
        if (($context["users"] ?? null)) {
            echo "            
                ";
            // line 32
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 33
                echo "                <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 33), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["user"], "username", [], "any", false, false, false, 33), "html", null, true);
                echo "</option>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "            
            ";
        }
        // line 36
        echo "        </select>
        <span><a href=\"/shouts/add\">Add new shout</a></span>
        <table id=\"shoutstbl\" border=\"1\" width=\"100%\">
            <thead>
                <tr>
                    <td>id of shout</td>
                    <td>author name</td>
                    <td>author avatar</td>
                    <td>message</td>
                </tr>
            </thead>
            <tbody id=\"shoutslist\">
                ";
        // line 48
        if (($context["shouts"] ?? null)) {
            echo "            
                ";
            // line 49
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["shouts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["shout"]) {
                // line 50
                echo "                <tr>
                    <td>";
                // line 51
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["shout"], "id", [], "any", false, false, false, 51), "html", null, true);
                echo "</td>
                    <td>";
                // line 52
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["shout"], "username", [], "any", false, false, false, 52), "html", null, true);
                echo "</td>
                    <td><image src=\"/uploads/";
                // line 53
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["shout"], "imagePath", [], "any", false, false, false, 53), "html", null, true);
                echo "\" height=\"50\"></image></td>
                    <td>";
                // line 54
                echo twig_get_attribute($this->env, $this->source, $context["shout"], "message", [], "any", false, false, false, 54);
                echo "</td>
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shout'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "            
            ";
        }
        // line 58
        echo "            </tbody>
        </table>
    </div>
";
    }

    public function getTemplateName()
    {
        return "shoutslist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 58,  162 => 56,  153 => 54,  149 => 53,  145 => 52,  141 => 51,  138 => 50,  134 => 49,  130 => 48,  116 => 36,  112 => 34,  101 => 33,  97 => 32,  93 => 31,  87 => 27,  83 => 26,  59 => 5,  55 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Shouts List{% endblock %}
{% block head %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"#users\").on('change', function() {
                //\$(\"#users option:selected\" ).text()
                if(\$(\"#users option:selected\" ).text()!=\"\"){
                    \$.get(\"/ajax/loadshouts/by/\"+\$(\"#users option:selected\" ).text()).done(function(data){
                        \$(\"#shoutslist\").html(data);
                    });
                }else{
                    \$.get(\"/ajax/loadshouts\").done(function(data){
                        \$(\"#shoutslist\").html(data);
                    });
                }
            });
        });
        \$(document).ajaxError(function (event, jqxhr, settings, thrownError) {
            alert(\"Ajax error occured\");
        });
    </script>
{% endblock %}
{% block content %}
    <div class=\"divcenter\"><h1>Welcome to Shouts</h1></div>
    <div class=\"divcenter\">        
        <select id=\"users\" name=\"users\">
            <option value=\"\"></option>
            {% if users %}            
                {% for user in users %}
                <option value=\"{{user.username}}\">{{user.username}}</option>
                {% endfor %}            
            {% endif %}
        </select>
        <span><a href=\"/shouts/add\">Add new shout</a></span>
        <table id=\"shoutstbl\" border=\"1\" width=\"100%\">
            <thead>
                <tr>
                    <td>id of shout</td>
                    <td>author name</td>
                    <td>author avatar</td>
                    <td>message</td>
                </tr>
            </thead>
            <tbody id=\"shoutslist\">
                {% if shouts %}            
                {% for shout in shouts %}
                <tr>
                    <td>{{shout.id}}</td>
                    <td>{{shout.username}}</td>
                    <td><image src=\"/uploads/{{shout.imagePath}}\" height=\"50\"></image></td>
                    <td>{{shout.message | raw }}</td>
                </tr>
                {% endfor %}            
            {% endif %}
            </tbody>
        </table>
    </div>
{% endblock %}", "shoutslist.html.twig", "C:\\xampp\\htdocs\\php\\quiz1slimshout\\templates\\shoutslist.html.twig");
    }
}
