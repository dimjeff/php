<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

require __DIR__ . '/vendor/autoload.php';

session_start();

DB::$user = 'day04slimfirst';
DB::$password = 'j6jGCwcCldL9Hqi9';
DB::$dbName = 'day04slimfirst';
DB::$port = 3306;
DB::$host = '127.0.0.1';

$app = AppFactory::create();

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ .'/cache', 'debug' => true]);

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'hello.html.twig', [
        'name' => $args['name']
    ]);
});

$app->get('/sayhello', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'sayhello.html.twig');
});

$app->post('/sayhello', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();
    $name = $postvars['name'];
    $age = $postvars['age'];
    $errList = array();
    if(strlen($name)<2||strlen($name)>20){
        array_push($errList,'name must be 2-20 chars');
        $postvars['name'] = "";
    }
    if($age<1||$age>150){
        array_push($errList,'age must be 1-150');
        $postvars['age'] = "";
    }
    if($errList){
        return $view->render($response, 'sayhello.html.twig', [
            'v' => $postvars, 'errList' => $errList
        ]);
    }else{
        DB::insert('people',['name'=>$name, 'age' => $age]);

        return $view->render($response, 'sayhello_success.html.twig', [
            'v' => $postvars
        ]); 
    }
    
});

$app->run();
?>