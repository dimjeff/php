<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>Document</title>
</head>
<body>
    <div id="centeredContent">
<?php
    require_once 'db.php';

    if(!isset($_GET['id'])){
        echo '<a href="/">error paramter</a>';
        exit();
    }
    echo '<a href="/">Home</a>';
    if(isset($_SESSION['user'])){ 
        echo '<div class="divright">Yor are logged in as '. $_SESSION['user']['username'].' <a href="logout.php">Logout</a></div>';           
    }else{
        echo '<div class="divright">you can <a href="login.php">Login</a> or <a href="register.php">Register</a> to post articles or comments</div>';
    }
    
    $articleid = $_GET['id'];

    function displayForm($content="",$articleid){
        $form = <<<MARKER
        <div class="divleft">My comment<div>
        <form method="post">
        <input type="submit" value="Create">
        <input hidden id="articleid" name="articleid" value="$articleid">
        Content: <textarea name="content" rows="4" cols="50">$content</textarea></br>        
    </form>
MARKER;
        echo $form;
    }

    $sql = sprintf("SELECT a.id,a.title,a.body,a.creationTime,u.username FROM article a inner join user u on a.authorId=u.id where a.id='%s'",
    mysqli_real_escape_string($conn,$articleid));

    $result = mysqli_query($conn,$sql);
    if(!$result){
        echo "Failed to execute MySQL query:" . mysqli_error($conn);
        exit();
    }
    
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $body = htmlspecialchars($row["body"]);
        $postdatetime=strtotime(htmlspecialchars($row["creationTime"]));

        echo '</br>';
        echo '<div> <h2><a href="articleaddedit.php?id='.$row["id"].'">'.htmlspecialchars($row["title"]).'</a></h2></div>';
        echo '<div> <h3>Posted by '.htmlspecialchars($row["username"]).' on '. date('M d, Y', $postdatetime) .' at '. date('H:i', $postdatetime)  .'</h3></div>';
        echo '<div class="divbody">'.$body.'</div>';
    }

    if(isset($_SESSION['user'])){ 
        //displayForm();
        if(isset($_POST['articleid'])){
            $articleid = $_POST['articleid'];
            $content = $_POST['content'];
            $errors= array();

            if(strlen($content)<5||strlen($content)>5000){
                $errors[]='content must be 5-5000 chars. ';
            }
            if(empty($errors)==true){                  
                $sql = sprintf("INSERT INTO comment(articleid,authorId,body) values('%s','%s','%s')",
                    mysqli_real_escape_string($conn,$articleid),
                    mysqli_real_escape_string($conn,$_SESSION['user']['id']),
                    mysqli_real_escape_string($conn,$content)
                );
                if(!mysqli_query($conn, $sql)){
                    echo "Failed to execute MySQL query:" . mysqli_error($conn);
                    exit();
                }
                echo "Successful, please go to homepage <a href='article.php?id=". $articleid ."'>CLick</a>.";
            }else{
                echo "<ul>";
                foreach($errors as $err)
                {
                    echo "<li>$err</li>";
                }
                echo "</ul>";
                displayForm($content,$articleid);
            }        
        }else{
            displayForm("",$articleid);
        }
    }    
    
    $results_per_page = 5; // number of results per page
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
    $start_from = ($page-1) * $results_per_page;
    $sql = sprintf("SELECT u.username,c.creationTime,c.body FROM comment c inner join user u on c.authorId = u.id where c.articleId=%s ORDER BY c.id DESC LIMIT %s, %s",
    mysqli_real_escape_string($conn,$articleid),
    mysqli_real_escape_string($conn,$start_from),
    mysqli_real_escape_string($conn,$results_per_page));
    $result = mysqli_query($conn,$sql);
    if(!$result){
        echo "Failed to execute MySQL query:" . mysqli_error($conn);
        exit();
    }
    
    if ($result->num_rows > 0) {
        echo '<div><h2>Previus comments</h2></div>';
        while($row = $result->fetch_assoc()) {
            $body = htmlspecialchars($row["body"]);
            $postdatetime=strtotime(htmlspecialchars($row["creationTime"]));

            echo '<div> <h3>'.htmlspecialchars($row["username"]).' said on '. date('M d, Y', $postdatetime) .' at '. date('H:i', $postdatetime)  .'</h3></div>';
            echo '<div> <h4>'.$body.'</div>';
            
            echo '</br>';
        }
    }
    $sql = sprintf("SELECT COUNT(ID) AS total FROM comment where articleid=%s",mysqli_real_escape_string($conn,$articleid)); 
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    $total_pages = ceil($row["total"] / $results_per_page);
    for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
        echo "<a href='article.php?id=". $articleid ."&page=".$i."'";
        if ($i==$page)  echo " class='curPage'";
        echo ">".$i."</a> "; 
    }; 

?>
    </div>
</body>
</html>

