<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* shoutsadd.html.twig */
class __TwigTemplate_ff730bea1464ac397f0404cf48b055840f7f625b2b836ec7f9246d0fd7cf12d1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "shoutsadd.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "add";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        if (($context["errors"] ?? null)) {
            // line 7
            echo "    <ul class=\"errorMessage\">
        ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["err"]) {
                // line 9
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["err"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['err'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 11
            echo "    </ul>
    ";
        }
        // line 13
        echo "    ";
        if (($context["userSession"] ?? null)) {
            // line 14
            echo "    <div class=\"divcenter\"><h2>add a message</h2><div>
        <form method=\"post\">
        <textarea name=\"message\" rows=\"10\" cols=\"50\">";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "body", [], "any", false, false, false, 16), "html", null, true);
            echo "</textarea></br>
        <input type=\"submit\" value=\"add\">
    </form>
    ";
        } else {
            // line 20
            echo "        please <a href=\"/login\">login</a> first.
    ";
        }
    }

    public function getTemplateName()
    {
        return "shoutsadd.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 20,  88 => 16,  84 => 14,  81 => 13,  77 => 11,  68 => 9,  64 => 8,  61 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}add{% endblock %}

{% block content %}
    {% if errors %}
    <ul class=\"errorMessage\">
        {% for err in errors %}
            <li>{{ err }}</li>
        {% endfor %}
    </ul>
    {% endif %}
    {% if userSession %}
    <div class=\"divcenter\"><h2>add a message</h2><div>
        <form method=\"post\">
        <textarea name=\"message\" rows=\"10\" cols=\"50\">{{ article.body }}</textarea></br>
        <input type=\"submit\" value=\"add\">
    </form>
    {% else %}
        please <a href=\"/login\">login</a> first.
    {% endif %}
{% endblock %}", "shoutsadd.html.twig", "C:\\xampp\\htdocs\\php\\quiz1slimshout\\templates\\shoutsadd.html.twig");
    }
}
