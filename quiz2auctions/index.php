<?php
use Slim\Factory\AppFactory;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require __DIR__ . '/vendor/autoload.php';

$app = AppFactory::create();

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

if (strpos($_SERVER['HTTP_HOST'], "ipd20.com") !== false) {
    // hosting on ipd20.com
    DB::$user = 'cp4966_teacher';
    DB::$password = 'UzXoLgOfibQ1Nk7n';
    DB::$dbName = 'cp4966_teacher';
    DB::$encoding = 'utf8'; // defaults to latin1 if omitted
} else { // local computer
    DB::$user = 'quiz2auctions';
    DB::$password = 'yaZEeIZcHo9StHkj';
    DB::$dbName = 'quiz2auctions';
    DB::$encoding = 'utf8'; // defaults to latin1 if omitted
}

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params)
{
    http_response_code(500);
    header('Content-Type: application/json');
    echo json_encode("Database error");
    global $log;
    $log->error("Database erorr[Connection]: " . $params['error']);
    if ($params['query']) {
        $log->error("Database error[Query]: " . $params['query']);
    }
    die();
}

// Add Error Middleware for 404 - not found handling
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$errorMiddleware->setErrorHandler(
    \Slim\Exception\HttpNotFoundException::class, 
        function () use ($app) {
            $response = $app->getResponseFactory()->createResponse();
            $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
            $response = $response->withStatus(404);
            $response->getBody()->write(json_encode("404 - not found"));
            return $response;
        }
);

$app->get('/', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Todo app with RESTful API");
    return $response;
});

$app->get('/auctions', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $paramArray = $request->getQueryParams();
    $sortBy = isset($paramArray['sortBy']) ? $paramArray['sortBy'] : "id";
    if (!in_array($sortBy, ['id', 'itemDesc', 'sellerEmail', 'lastBid', 'lastBidderEmail'])) {
        //global $log;
        //$log->debug("GET /auction failed due to invalid sortBy from " .  $_SERVER['REMOTE_ADDR'] . ": " . $sortBy);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - invalid sortBy value"));
        return $response;
    }
    $todoList = DB::query("SELECT id, itemDesc, sellerEmail, lastBid, lastBidderEmail FROM auctions ORDER BY %l", $sortBy);
    $json = json_encode($todoList, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

$app->map(['PUT', 'PATCH'],'/auctions/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $json = $request->getBody();
    $auction = json_decode($json, true);
    $id = $args['id'];
    if ( ($result = validateAuction($auction, false)) !== TRUE) {
        global $log;
        $log->debug("PATCH /auction failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    global $log;
    $lastbid = DB::queryFirstField("SELECT lastBid FROM auctions WHERE id=%s", $id);
    if(!$lastbid) {        
        $result = "Record with id $id does not exist";
        $log->debug("PATCH /auction failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - " . $result));
        return $response;
    }
    if($auction['lastbid']<=$lastbid){
        $result = "your bid must greater than last bid with id $id";
        $log->debug("PATCH /auction failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - " . $result));
        return $response;
    }
    DB::update('auctions', $auction, "id=%s", $id);
    $response->getBody()->write(json_encode(true));
    return $response;
});

$app->get('/auctions/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $id = $args['id'];
    $auction = DB::queryFirstRow("SELECT id, itemDesc, sellerEmail, lastBid, lastBidderEmail FROM auctions WHERE id=%s", $id);
    if (!$auction) {
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    }
    $json = json_encode($auction, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

$app->post('/auctions', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $json = $request->getBody();
    $auction = json_decode($json, true);

    if ( ($result = validateAuction($auction, true)) !== TRUE) {
        global $log;
        $log->debug("POST /auction failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }

    DB::insert('auctions', $auction);
    $id = DB::insertId();
    $response = $response->withStatus(201); // record created
    $response->getBody()->write(json_encode($id));
    return $response;
});

function validateAuction($auction,$add) {
    global $log;
    if ($auction === NULL) {
        return "Invalid JSON data provided";
    }
    $expectedFields = ['itemDesc', 'sellerEmail', 'lastBid', 'lastBidderEmail'];
    $auctionFields = array_keys($auction);

    if (($diff = array_diff($auctionFields, $expectedFields))) {
        return "Invalid fields in auction: [" . implode(',',$diff) . "]";
    }

    foreach ($auction as $key => $value) {
        if (@is_null($value)) {
            return "$key must not be null";
        }
    }

    if (isset($auction['sellerEmail'])) {
        if (is_null($auction['sellerEmail'])) {
            return "Seller Email must not be null";
        }
        $email = $auction['sellerEmail'];
        if (strlen($email) < 1 || strlen($email) > 250) {
            return "Seller Email must be 1-250 characters long";
        }
        if(filter_var($email, FILTER_VALIDATE_EMAIL)==FALSE){
            return 'Seller Email does not look valid.';
        }
    }
    if (isset($auction['itemDesc'])) {
        $itemDesc = $auction['itemDesc'];
        if (strlen($itemDesc) < 1 || strlen($itemDesc) > 200) {
            return "Item description must be 1-200 characters long";
        }
    }

    if($add){
        if (isset($auction['lastBid'])) {
            $lastBid = $auction['lastBid'];
            if (!is_numeric($lastBid) || $lastBid!=0) {
                return "New auction LastBid must be 0";
            }
        }
        if (isset($auction['lastBidderEmail'])) {
            $email = $auction['lastBidderEmail'];
            if (strlen($email)>0) {
                return "lastBidderEmail must be empty";
            }
        }
    }else{
        if (isset($auction['lastBid'])) {
            $lastBid = $auction['lastBid'];
            if (is_numeric($lastBid)) { 
                return "LastBid must be a number";
            } 
            if ($lastBid<=0) {
                return "LastBid must greater than 0";
            }
        }
        if (isset($auction['lastBidderEmail'])) {
            $email = $auction['lastBidderEmail'];
            if (strlen($email)==0) {
                return "lastBidderEmail can not be empty";
            }
        }
    }

    return TRUE;
}

$app->run();

?>