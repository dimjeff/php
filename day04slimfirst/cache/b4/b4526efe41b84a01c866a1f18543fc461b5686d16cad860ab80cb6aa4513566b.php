<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sayhello.html.twig */
class __TwigTemplate_e5dea91ee690975ebab19207be3a9e5a732a894f62dac15d3263df3065ff83b2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "sayhello.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Say Hello";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        if (($context["errList"] ?? null)) {
            // line 7
            echo "    <ul class=\"errorMessage\">
        ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["err"]) {
                // line 9
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["err"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['err'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 11
            echo "    </ul>
";
        }
        // line 13
        echo "    <form method=\"post\">
        Name: <input type=\"text\" name=\"name\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "name", [], "any", false, false, false, 14), "html", null, true);
        echo "\"></br>
        Age: <input type=\"number\" name=\"age\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "age", [], "any", false, false, false, 15), "html", null, true);
        echo "\"></br>
        <input type=\"submit\" value=\"Say Hello\">
    </form>
";
    }

    public function getTemplateName()
    {
        return "sayhello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 15,  83 => 14,  80 => 13,  76 => 11,  67 => 9,  63 => 8,  60 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Say Hello{% endblock %}

{% block content %}
{% if errList %}
    <ul class=\"errorMessage\">
        {% for err in errList %}
            <li>{{ err }}</li>
        {% endfor %}
    </ul>
{% endif %}
    <form method=\"post\">
        Name: <input type=\"text\" name=\"name\" value=\"{{ v.name}}\"></br>
        Age: <input type=\"number\" name=\"age\" value=\"{{ v.age}}\"></br>
        <input type=\"submit\" value=\"Say Hello\">
    </form>
{% endblock %}", "sayhello.html.twig", "C:\\xampp\\htdocs\\php\\day04slimfirst\\templates\\sayhello.html.twig");
    }
}
