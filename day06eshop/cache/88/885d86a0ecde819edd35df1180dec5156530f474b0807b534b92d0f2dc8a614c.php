<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* article.html.twig */
class __TwigTemplate_4cefebfa2392b66e81d4392e591c6fca871021c9a595586dccfd4d3bcec899d1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "article.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "My Blog - Article";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        if (($context["article"] ?? null)) {
            // line 7
            echo "    </br>
    <div> <h2 class=\"divcenter\"><a href=\"/articleaddedit/";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "id", [], "any", false, false, false, 8), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "title", [], "any", false, false, false, 8), "html", null, true);
            echo "</a></h2></div>
    <div> <h3>Posted by ";
            // line 9
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "author", [], "any", false, false, false, 9), "html", null, true);
            echo " on ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "postdate", [], "any", false, false, false, 9), "html", null, true);
            echo " at ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "posttime", [], "any", false, false, false, 9), "html", null, true);
            echo "</h3></div>
    <div> <h4> ";
            // line 10
            echo twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "body", [], "any", false, false, false, 10);
            echo " </div>
";
        }
        // line 12
        if (($context["userSession"] ?? null)) {
            // line 13
            echo "    <div class=\"divleft\">My comment<div>
    ";
            // line 14
            if (($context["error"] ?? null)) {
                // line 15
                echo "        <ul class=\"errorMessage\">
            <li>";
                // line 16
                echo twig_escape_filter($this->env, ($context["error"] ?? null), "html", null, true);
                echo "</li>
        </ul>
    ";
            }
            // line 19
            echo "    <form method=\"post\">
        <input type=\"submit\" value=\"Create\">
        <input hidden id=\"articleid\" name=\"articleid\" value=\"";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "id", [], "any", false, false, false, 21), "html", null, true);
            echo "\">
        Content: <textarea name=\"comment\" rows=\"4\" cols=\"50\">";
            // line 22
            if (($context["error"] ?? null)) {
                echo twig_escape_filter($this->env, ($context["comment"] ?? null), "html", null, true);
            }
            echo "</textarea></br>        
    </form>
";
        }
        // line 25
        echo "
";
        // line 26
        if (($context["comments"] ?? null)) {
            // line 27
            echo "    <div><h2>Previus comments</h2></div>
    ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["comments"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["comm"]) {
                // line 29
                echo "    <div> <h3>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comm"], "author", [], "any", false, false, false, 29), "html", null, true);
                echo " said on ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comm"], "postdate", [], "any", false, false, false, 29), "html", null, true);
                echo " at ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comm"], "posttime", [], "any", false, false, false, 29), "html", null, true);
                echo "</h3></div>
    <div> <h4>";
                // line 30
                echo twig_get_attribute($this->env, $this->source, $context["comm"], "body", [], "any", false, false, false, 30);
                echo "</div>
    </br>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comm'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 34
        echo "
";
        // line 35
        if (($context["totalpages"] ?? null)) {
            // line 36
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, ($context["totalpages"] ?? null)));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 37
                echo "        <a href=\"/article/";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "id", [], "any", false, false, false, 37), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "\" ";
                if (0 === twig_compare($context["page"], ($context["currentpage"] ?? null))) {
                    echo "class=\"errorMessage";
                }
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</a>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
    }

    public function getTemplateName()
    {
        return "article.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 37,  150 => 36,  148 => 35,  145 => 34,  135 => 30,  126 => 29,  122 => 28,  119 => 27,  117 => 26,  114 => 25,  106 => 22,  102 => 21,  98 => 19,  92 => 16,  89 => 15,  87 => 14,  84 => 13,  82 => 12,  77 => 10,  69 => 9,  63 => 8,  60 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}My Blog - Article{% endblock %}

{% block content %}
{% if article %}
    </br>
    <div> <h2 class=\"divcenter\"><a href=\"/articleaddedit/{{ article.id }}\">{{ article.title }}</a></h2></div>
    <div> <h3>Posted by {{ article.author }} on {{ article.postdate }} at {{ article.posttime }}</h3></div>
    <div> <h4> {{ article.body | raw }} </div>
{% endif %}
{% if userSession %}
    <div class=\"divleft\">My comment<div>
    {% if error %}
        <ul class=\"errorMessage\">
            <li>{{ error }}</li>
        </ul>
    {% endif %}
    <form method=\"post\">
        <input type=\"submit\" value=\"Create\">
        <input hidden id=\"articleid\" name=\"articleid\" value=\"{{article.id}}\">
        Content: <textarea name=\"comment\" rows=\"4\" cols=\"50\">{% if error %}{{ comment }}{% endif %}</textarea></br>        
    </form>
{% endif %}

{% if comments %}
    <div><h2>Previus comments</h2></div>
    {% for comm in comments %}
    <div> <h3>{{comm.author}} said on {{comm.postdate}} at {{ comm.posttime }}</h3></div>
    <div> <h4>{{ comm.body | raw }}</div>
    </br>
    {% endfor %}
{% endif %}

{% if totalpages %}
    {% for page in 1..totalpages %}
        <a href=\"/article/{{article.id}}/{{page}}\" {% if page == currentpage%}class=\"errorMessage{% endif %}\">{{ page }}</a>
    {% endfor %}
{% endif %}
{% endblock %}", "article.html.twig", "C:\\xampp\\htdocs\\php\\day04slimblog\\templates\\article.html.twig");
    }
}
