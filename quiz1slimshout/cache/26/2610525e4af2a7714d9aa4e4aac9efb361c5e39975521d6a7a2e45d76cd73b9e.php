<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* articleaddedit.html.twig */
class __TwigTemplate_bc2757a1a031a65c7a5ea882fa8a2e3096a3a7576d970d91c8ea6a05ade37be3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "articleaddedit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "My Blog - Add/Edit article";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        if (($context["errors"] ?? null)) {
            // line 7
            echo "    <ul class=\"errorMessage\">
        ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["err"]) {
                // line 9
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["err"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['err'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 11
            echo "    </ul>
    ";
        }
        // line 13
        echo "    ";
        if (($context["user"] ?? null)) {
            // line 14
            echo "    <div class=\"divcenter\"><h2>Create/Edit article</h2><div>
        <form method=\"post\">
        Title: <input type=\"text\" name=\"title\" value=\"";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "title", [], "any", false, false, false, 16), "html", null, true);
            echo "\"></br>
        Content: <textarea name=\"content\" rows=\"4\" cols=\"50\">";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["article"] ?? null), "body", [], "any", false, false, false, 17), "html", null, true);
            echo "</textarea></br>
        <input type=\"submit\" value=\"Create/Edit\">
    </form>
    ";
        }
    }

    public function getTemplateName()
    {
        return "articleaddedit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 17,  88 => 16,  84 => 14,  81 => 13,  77 => 11,  68 => 9,  64 => 8,  61 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}My Blog - Add/Edit article{% endblock %}

{% block content %}
    {% if errors %}
    <ul class=\"errorMessage\">
        {% for err in errors %}
            <li>{{ err }}</li>
        {% endfor %}
    </ul>
    {% endif %}
    {% if user %}
    <div class=\"divcenter\"><h2>Create/Edit article</h2><div>
        <form method=\"post\">
        Title: <input type=\"text\" name=\"title\" value=\"{{ article.title }}\"></br>
        Content: <textarea name=\"content\" rows=\"4\" cols=\"50\">{{ article.body }}</textarea></br>
        <input type=\"submit\" value=\"Create/Edit\">
    </form>
    {% endif %}
{% endblock %}", "articleaddedit.html.twig", "C:\\xampp\\htdocs\\php\\day04slimblog\\templates\\articleaddedit.html.twig");
    }
}
