<?php
use DI\Container;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require __DIR__ . '/vendor/autoload.php';

session_start();

$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'quiz1slimshout20';
DB::$password = 'AnbCKLBxm2X42jRh';
DB::$dbName = 'quiz1slimshout20';
DB::$port = 3306;
DB::$host = '127.0.0.1';
DB::$param_char = ':';
DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    header("Location: /internalerror");
    global $log;
    $log->error("database error:". $params['error']);
    if(isset($params['query'])){
        $log->error("SQL query error:".$params['query']);
    };
    die;
  }

$container = new Container();
$container->set('upload_directory', __DIR__ . '/uploads');
AppFactory::setContainer($container);
$app = AppFactory::create();

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ .'/cache', 'debug' => true]);
$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user'])?$_SESSION['user']:false);

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));

$app->get('/internalerror', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/forbidden', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    //print_r($_SESSION['user']);
    //return $response;
    return $view->render($response, 'index.html.twig');
});

$app->get('/checkusernameavailable/[{username}]', function (Request $request, Response $response, array $args) {
    $username = isset($args['username']) ? $args['username'] : "";
    $results = DB::queryFirstRow("SELECT id FROM users WHERE username=:s", $username);
    if($results){
        echo "false";
    }else{        
        echo "true";
    }

    return $response;
});

$app->get('/ajax/loadshouts[/by/{username}]', function (Request $request, Response $response, array $args) {
    $username = isset($args['username']) ? $args['username'] : "";
    $results=null;
    //echo $username;
    if($username)
    {
        $results = DB::query("SELECT s.id, u.username, u.imagePath, s.message FROM shouts s, users u WHERE s.authorId=u.id and u.username=:s ORDER BY s.id desc", $username);
    }
    else{
        $results = DB::query("SELECT s.id, u.username, u.imagePath, s.message FROM shouts s, users u WHERE s.authorId=u.id ORDER BY s.id desc");
     }
    //print_r($results);
    if(!$results){
        echo "";
    }else{
        foreach ($results as $row) {
            echo "<tr>";
            echo "<td>" . $row['id'] . "</td>";
            echo "<td>" . $row['username'] . "</td>";
            echo "<td> <image src='/uploads/" . $row['imagePath'] . "' height='50'></image></td>";
            echo "<td> " . $row['message'] . "</td>";
            echo "</tr>";
          }
    }

    return $response;
});

$app->get('/logout', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    unset($_SESSION['user']);
    return $view->render($response, 'logout.html.twig');
});

$app->get('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'login.html.twig');
});

$app->post('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();

    $username = isset($postvars['username']) ? $postvars['username'] : "";;
    $password = isset($postvars['password']) ? $postvars['password'] : "";;

    $user = DB::queryFirstRow("SELECT * FROM users WHERE username=:s", $username);
    if(!$user){
        if($user['password']!=$password){
            $error = "Invalid username or password, please check again.";
            return $view->render($response, 'login.html.twig', [
                'v' => $postvars, 'error' => $error
            ]);
        }
    }else{    
        unset($user['password']);
        $_SESSION['user']= $user;
        echo "Login successful, please to <a href='/'>Home</a>.";
        return $response;
    }
});

$app->get('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig');
});

$app->get('/shouts/add', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/forbidden')->withStatus(302);
    }
    return $view->render($response, 'shoutsadd.html.twig');
});

$app->post('/shouts/add', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/forbidden')->withStatus(302);
    }
    $postvars = $request->getParsedBody();
    $errors= array();
    $message = isset($postvars['message']) ? $postvars['message'] : "";
    $message = strip_tags($message, "<p><ul><li><em><strong><i><b><ol><h3><h4><span>");
    if (strlen($message) < 1 || strlen($message) > 100) {
        array_push($errors, "Message must be 1-100 characters long");
    }
    if ($errors) {
        return $view->render($response, 'shoutsadd.html.twig', [
            'v' => $postvars, 'errors' => $errors
        ]);
    }
    else{
        $authorId = $_SESSION['user']['id']; // ID of currently logged in user
        DB::insert('shouts', [ 'authorId' => $authorId, 'message' => $message ]);
        //$newId = DB::insertId();
        echo "<p>add Successful, to view message click <a href='/shouts/list'>here</a>.</p>";
        return $response;
    }

});

$app->get('/shouts/list', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $shouts = DB::query("SELECT s.id, u.username, u.imagePath, s.message FROM shouts s, users u where s.authorId=u.id order by s.id desc");
    $users = DB::query("SELECT username FROM users order by username");
    return $view->render($response, 'shoutslist.html.twig',['shouts' => $shouts, 'users' => $users]);
});

$app->post('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();
    $errors= array();

    $username = isset($postvars['username']) ? $postvars['username'] : "";
    $password = isset($postvars['password']) ? $postvars['password'] : "";
    $confirmpwd = isset($postvars['confirm_password']) ? $postvars['confirm_password'] : "";

    $result = file_get_contents("http://quiz1slimshout/checkusernameavailable/$username");

    if($result != "true"){
        $errors[]='This username has already been taken by another user.';
        $postvars['username']="";
    }else{
        if (preg_match('/^[a-zA-Z][a-zA-Z0-9_]{5,20}$/', $username) != 1) {
            $errors[]="Username must be 6-20 characters long, begin with a letter and only consist of uppercase/lowercase letters, digits, and underscores";
            $postvars['username'] = '';
        }
    }

    if($password!=$confirmpwd){
        $errors[]="Passwords do not match";
        $postvars['password']="";
    }else{
        if ((strlen($password) < 5) || (strlen($password) > 100)
        || (preg_match("/[A-Z]/", $password) == FALSE )
        || (preg_match("/[a-z]/", $password) == FALSE )
        || (preg_match("/[0-9]/", $password) == FALSE )) {
            $errors[]="Password must be at least 6 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it";
        }
    }

    $directory = $this->get('upload_directory');
    $uploadedFiles = $request->getUploadedFiles();
    $uploadedFile = $uploadedFiles['photo'];
    //print_r($uploadedFile);
    //echo $uploadedFile->filesize();
    
    $filename = "";
    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
        $filename = moveUploadedFile($directory, $uploadedFile);
        $imageInfo = getimagesize($directory."/".$filename);
        $width = $imageInfo[0];
        $height = $imageInfo[1];
        $mimeType = $imageInfo['mime'];

        if($mimeType=='image/jpeg' || $mimeType=='image/png' || $mimeType=='image/gif'){
            if ($width < 50 || $width > 500 || $height < 50 || $height > 500) {
                $errors[] = "Image width and height must be in 50-500 pixels range";
                unlink($directory."/".$filename);//delete file
            }
        }else
        {
            $errors[]="Image must be gif, png or jpeg type";
            unlink($directory."/".$filename);//delete file
        }
        
    } else {
        $errors[]='upload failed';
    }
    //print_r($errors);
    //return $response;

    if($errors){
        return $view->render($response, 'register.html.twig', [ 'v' => $postvars, 'errors' => $errors  ]);
    }else{
        DB::insert('users',['username'=>$username, 'imagePath' => $filename, 'password' => $password]);
        return $view->render($response, 'register_success.html.twig', [ 'v' => $postvars ]);
    } 
});

function moveUploadedFile($directory, UploadedFileInterface $uploadedFile)
{
    //getUploadedPath() { return $this->file; }
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8));
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}

$app->addErrorMiddleware(true, true, true);

$app->run();
?>