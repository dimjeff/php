<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>Document</title>
</head>
<body>
    <div id="centeredContent">
    <?php
        require_once 'db.php';
        if(!isset($_SESSION['user'])){
            echo '<div class="divright">you can <a href="login.php">Login</a> or <a href="register.php">Register</a> to post articles or comments</div>';
        }else{
            echo '<div class="divright">Yor are logged in as '. $_SESSION['user']['username'].'</div>';
            echo '<div class="divright">You can <a href=logout.php>logout</a> or <a href=articleaddedit.php>post an article</a></div>';
        }

        echo '<div class="divcenter"><h1>Welcome to my blog</h1></div>';
        $results_per_page = 5; // number of results per page
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
        $start_from = ($page-1) * $results_per_page;
        $sql = "SELECT a.id,a.title,a.body,a.creationTime,u.username FROM article a inner join user u on a.authorId=u.id ORDER BY a.id DESC LIMIT $start_from, ".$results_per_page;
        $result = mysqli_query($conn,$sql);
        if(!$result){
            echo "Failed to execute MySQL query:" . mysqli_error($conn);
            exit();
        }
        
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $body = htmlspecialchars($row["body"]);
                $body = substr($body,0,50)."......";
                $postdatetime=strtotime(htmlspecialchars($row["creationTime"]));

                echo '</br>';
                echo '<div> <h2><a href="article.php?id='.$row["id"].'">'.htmlspecialchars($row["title"]).'</a></h2></div>';
                echo '<div> <h3>Posted by '.htmlspecialchars($row["username"]).' on '. date('M d, Y', $postdatetime) .' at '. date('H:i', $postdatetime)  .'</h3></div>';
                echo '<div> <h4>'.$body.'</div>';
            }
        }
        $sql = "SELECT COUNT(ID) AS total FROM article"; 
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();
        $total_pages = ceil($row["total"] / $results_per_page);
        for ($i=1; $i<=$total_pages; $i++) {  // print links for all pages
            echo "<a href='index.php?page=".$i."'";
            if ($i==$page)  echo " class='curPage'";
            echo ">".$i."</a> "; 
        }; 
    ?>
    </div>
</body>
</html>