<?php

function db_viewallpassports(){
  $servername = "localhost"; //localhost:3333
  $username = "root";
  $password = "";
  $dbname = "day01people";
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT id, passportNo, photoFilePath FROM passports";
    $result = $conn->query($sql);
    
    $strhtml = '';
    if ($result->num_rows > 0) {
      $strhtml .= '<table border=1><thead><tr><th>id</th><th>passportNo</th><th>photo</th></tr></thead><tbody>';
        while($row = $result->fetch_assoc()) {
          $strhtml .=  '<tr><td>' . htmlspecialchars($row["id"]) . '</td><td>'. htmlspecialchars($row["passportNo"]) . '</td><td><image src="uploads/'. 
          htmlspecialchars($row["photoFilePath"]) . '"></td></tr>';
        }
      $strhtml .= '</tbody></table>';
    } else {
      $strhtml =  "0 results";
    }

    $conn->close();
    return $strhtml;
}

function db_addpassport($passportno,$photofile){
  $servername = "localhost"; //localhost:3333
  $username = "root";
  $password = "";
  $dbname = "day01people";
  $msg = "";
  try{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("INSERT INTO passports (passportNo, photoFilePath) VALUES (:passportno, :photofile)");
    $stmt->bindParam(':passportno', $passportno);
    $stmt->bindParam(':photofile', $photofile);
    $stmt->execute();

    $msg = "New records created successfully";
  }
  catch(PDOException $e)
  {
    $msg = "Error: " . $e->getMessage();
  }
  $conn = null;
  return $msg;
}

function db_insert(Person $p){
  $servername = "localhost"; //localhost:3333
  $username = "root";
  $password = "";
  $dbname = "day01people";
  $msg = "";
  try{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $name = $p->get_name();
    $gpa = $p->get_gpa();
    $isgrad = $p->get_isgrad();
    $gender = $p->get_gender();

    $stmt = $conn->prepare("INSERT INTO people (name, gpa, isGraduate, gender) VALUES (:name, :gpa, :isGraduate, :gender)");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':gpa', $gpa);
    $stmt->bindParam(':isGraduate', $isgrad);
    $stmt->bindParam(':gender', $gender);
    $stmt->execute();

    $msg = "New records created successfully";
  }
  catch(PDOException $e)
  {
    $msg = "Error: " . $e->getMessage();
  }
  $conn = null;
    /*if ($conn->query($sql) === TRUE) {
        $msg = "New record created successfully";
    } else {
        $msg = "Error: " . $sql . "<br>" . $conn->error;
    }*/
    
    return $msg;
}

function db_select(){
  $servername = "localhost"; //localhost:3333
  $username = "root";
  $password = "";
  $dbname = "day01people";
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT id, name, gpa, isGraduate, gender FROM people";
    $result = $conn->query($sql);
    
    $strhtml = '';
    if ($result->num_rows > 0) {
      $strhtml .= '<table border=1><thead><tr><th>id</th><th>name</th><th>gpa</th><th>isGraduate</th><th>gender</th></tr></thead><tbody>';
        while($row = $result->fetch_assoc()) {
          $strhtml .=  '<tr><td>' . htmlspecialchars($row["id"]) . '</td><td>'. htmlspecialchars($row["name"]) . '</td><td>'. htmlspecialchars($row["gpa"]) . '</td><td>'. $row["isGraduate"] . '</td><td>'. $row["gender"] . '</td></tr>';
        }
      $strhtml .= '</tbody></table>';
    } else {
      $strhtml =  "0 results";
    }

    $conn->close();
    return $strhtml;
}

class Person {
    // Properties
    private $name;
    private $gpa;
    private $isgrad;
    private $gender;
  
    // Methods
    function set_name($name) {
      $this->name = $name;
    }
    function get_name() {
      return $this->name;
    }

    function set_gpa($gpa) {
        $this->gpa = $gpa;
      }
      function get_gpa() {
        return $this->gpa;
      }

      function set_isgrad($isgrad) {
        $this->isgrad = $isgrad;
      }
      function get_isgrad() {
        return $this->isgrad;
      }

      function set_gender($gender) {
        $this->gender = $gender;
      }
      function get_gender() {
        return $this->gender;
      }
  }

?>
