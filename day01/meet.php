<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" herf="styles.css" />
    <title>Document</title>
</head>
<body>
    <?php
    function displayForm(){
        $form = <<<MARKER
        <form method="post">
        Name: <input type="text" name="name"></br>
        Age: <input type="number" name="age"></br>
        <input type="submit" value="Append to file">
    </form>
MARKER;
        echo $form;
    }

    if(isset($_POST['age']))
    {
        $name = $_POST['name'];
        $age = $_POST['age'];
        $errList = array();

        if(strlen($name)<2||strlen($name)>20){
            array_push($errList,'name must be 2-20 chars');
        }
        if($age<1||$age>150){
            array_push($errList,'age must be 1-150');
        }

        if($errList){
            echo "<ul>";
            foreach($errList as $err)
            {
                echo "<li>$err</li>";
            }
            echo "</ul>";
            displayForm();
        }else{
            $fp = fopen('people.txt', 'a');//opens file in append mode  
            fwrite($fp, $name.";".$age.PHP_EOL);  
            fclose($fp);  
            
            echo "successful submission.";  
        }
    }else{
        displayForm();
    }
    ?>
</body>
</html>