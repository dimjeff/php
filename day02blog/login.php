<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <title>Document</title>
</head>
<body>
<div class="form-style-10">
    <?php
    require_once 'db.php';
    function displayForm($username=""){
        $form = <<<MARKER
        <form method="post" id="signup" >
        <div class="inner-warp">
            <label for="username">Username</label>
            <input type="text" id="username" name="username" maxlength="50" required="required" value="$username"/><br/>
            <label for="password">Password</label>
            <input type="password" id="password" name="password" required="required"/>
        </div>
        <div class="divcenter">
            <input type="submit" id="login" name="login" value="Login"/>
            <input type="button" id="home" name="home" value="Home" onclick="window.location.href = '/';"/>
        </div>
    </form>
MARKER;
        echo $form;
    }

    if(isset($_POST['username']))
    {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $loginSuccessful = false;

        $result = mysqli_query($conn,sprintf("SELECT * FROM user WHERE username='%s'",
                mysqli_real_escape_string($conn,$username)));
        if(!$result){
            echo "SQL Query failed:" . mysqli_error($conn);
            exit;
        }
        $user = mysqli_fetch_assoc($result);
        if($user){
            if($user['password']==$password){
                $loginSuccessful = true;
            }
        }
        if(!$loginSuccessful){
            echo "<p class=errorMessage>Login failed</p>\n";
            displayForm($username);
        }else{
            echo "<p>Login successful</p>";
            echo '<p><a href="index.php">Click to continue</a>';
            unset($user['password']);
            $_SESSION['user']= $user;
        }

    }else{
        displayForm();
    }
    ?>
    </div>
</body>
</html>