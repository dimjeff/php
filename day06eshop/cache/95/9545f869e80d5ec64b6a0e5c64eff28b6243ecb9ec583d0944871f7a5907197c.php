<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* allcategories.html.twig */
class __TwigTemplate_32ab6e3c010a98386b1c7dce828b0614c8c1b23507dc15faf6c7c9ed6bf21aaf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "shop.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("shop.html.twig", "allcategories.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Welcome to eShop - ";
        echo twig_escape_filter($this->env, ($context["pagetitle"] ?? null), "html", null, true);
    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "<style>
    #tishi{
    margin: 0 auto;
    border:2px solid #FFFFFF;
    font-size:22px;
    color:#990000;
    padding-top:20px;
    width:100%;
    height:100%;
    position:absolute;
    z-index:110;
    display:none;
    background:#e7e7e7;
    left:50%-120;
    top:0;
    opacity:0.8;
    text-align: center;
}
</style>
";
    }

    // line 25
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "<div id=\"tishi\">
    Add a category<br/>
    <form method=\"post\">
        Name: <input type=\"text\" name=\"name\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "name", [], "any", false, false, false, 29), "html", null, true);
        echo "\"></br>
        <input type=\"submit\">
    </form>
</div>
    <div class=\"cart_buttons\">
        <button type=\"button\" class=\"button cart_button_clear\">Delete Category</button>
        <button type=\"button\" class=\"button cart_button_checkout\" onclick=\"document.getElementById('tishi').style.display = 'block';\">Add Category</button>
    </div>

    ";
        // line 38
        if (($context["categories"] ?? null)) {
            // line 39
            echo "    <table border=\"1\" width=\"100%\">
        <thead>
            <tr>
                <th>id</th>
                <th>name</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        ";
            // line 48
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 49
                echo "        <tr>
            <td>
                ";
                // line 51
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 51), "html", null, true);
                echo "
            </td>
            <td>
                ";
                // line 54
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 54), "html", null, true);
                echo "
            </td>
            <td>
                <span>delete</span>
            </td>
        </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 61
            echo "        </tbody>
    </table>
        ";
            // line 63
            if (($context["totalpages"] ?? null)) {
                // line 64
                echo "            <div class=\"shop_page_nav d-flex flex-row\">
                <div class=\"page_prev d-flex flex-column align-items-center justify-content-center\"><i class=\"fas fa-chevron-left\"></i></div>
                <ul class=\"page_nav d-flex flex-row\">
                    ";
                // line 67
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, ($context["totalpages"] ?? null)));
                foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                    // line 68
                    echo "                    <li><a href=\"#\">";
                    echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                    echo "</a></li>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 70
                echo "                </ul>
                <div class=\"page_next d-flex flex-column align-items-center justify-content-center\"><i class=\"fas fa-chevron-right\"></i></div>
            </div>
        ";
            }
            // line 74
            echo "
        
    ";
        }
        // line 76
        echo " 

    
";
    }

    public function getTemplateName()
    {
        return "allcategories.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 76,  174 => 74,  168 => 70,  159 => 68,  155 => 67,  150 => 64,  148 => 63,  144 => 61,  131 => 54,  125 => 51,  121 => 49,  117 => 48,  106 => 39,  104 => 38,  92 => 29,  87 => 26,  83 => 25,  60 => 5,  56 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"shop.html.twig\" %}

{% block title %}Welcome to eShop - {{pagetitle}}{% endblock %}
{% block head %}
<style>
    #tishi{
    margin: 0 auto;
    border:2px solid #FFFFFF;
    font-size:22px;
    color:#990000;
    padding-top:20px;
    width:100%;
    height:100%;
    position:absolute;
    z-index:110;
    display:none;
    background:#e7e7e7;
    left:50%-120;
    top:0;
    opacity:0.8;
    text-align: center;
}
</style>
{% endblock %}
{% block content %}
<div id=\"tishi\">
    Add a category<br/>
    <form method=\"post\">
        Name: <input type=\"text\" name=\"name\" value=\"{{ v.name}}\"></br>
        <input type=\"submit\">
    </form>
</div>
    <div class=\"cart_buttons\">
        <button type=\"button\" class=\"button cart_button_clear\">Delete Category</button>
        <button type=\"button\" class=\"button cart_button_checkout\" onclick=\"document.getElementById('tishi').style.display = 'block';\">Add Category</button>
    </div>

    {% if categories %}
    <table border=\"1\" width=\"100%\">
        <thead>
            <tr>
                <th>id</th>
                <th>name</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        {% for category in categories %}
        <tr>
            <td>
                {{category.id}}
            </td>
            <td>
                {{category.name}}
            </td>
            <td>
                <span>delete</span>
            </td>
        </tr>
        {% endfor %}
        </tbody>
    </table>
        {% if totalpages %}
            <div class=\"shop_page_nav d-flex flex-row\">
                <div class=\"page_prev d-flex flex-column align-items-center justify-content-center\"><i class=\"fas fa-chevron-left\"></i></div>
                <ul class=\"page_nav d-flex flex-row\">
                    {% for page in 1..totalpages %}
                    <li><a href=\"#\">{{ page }}</a></li>
                    {% endfor %}
                </ul>
                <div class=\"page_next d-flex flex-column align-items-center justify-content-center\"><i class=\"fas fa-chevron-right\"></i></div>
            </div>
        {% endif %}

        
    {% endif %} 

    
{% endblock %}", "allcategories.html.twig", "C:\\xampp\\htdocs\\php\\day06eshop\\templates\\allcategories.html.twig");
    }
}
