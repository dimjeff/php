<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_e989d30591416b76ed4979b3d2a20be296a126417f9db421860e5fb9a72a72ea extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Home";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <div class=\"divcenter\"><h1>Welcome to Shouts</h1></div>
    <div class=\"divcenter\"><h3><a href=\"/login\">login</a></h3></div>
    <div class=\"divcenter\"><h3><a href=\"/register\">register</a></h3></div>
    <div class=\"divcenter\"><h3><a href=\"/logout\">logout</a></h3></div>
    <div class=\"divcenter\"><h3><a href=\"/shouts/list\">shouts list</a></h3></div>
";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Home{% endblock %}

{% block content %}
    <div class=\"divcenter\"><h1>Welcome to Shouts</h1></div>
    <div class=\"divcenter\"><h3><a href=\"/login\">login</a></h3></div>
    <div class=\"divcenter\"><h3><a href=\"/register\">register</a></h3></div>
    <div class=\"divcenter\"><h3><a href=\"/logout\">logout</a></h3></div>
    <div class=\"divcenter\"><h3><a href=\"/shouts/list\">shouts list</a></h3></div>
{% endblock %}", "index.html.twig", "C:\\xampp\\htdocs\\php\\quiz1slimshout\\templates\\index.html.twig");
    }
}
