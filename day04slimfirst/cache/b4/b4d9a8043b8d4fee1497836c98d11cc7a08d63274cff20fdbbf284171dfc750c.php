<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_b5e37d6f3d875171550d3f1e2c3419a20e84a59615b65f0aef15fb5e848fa1fa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>        
        <link rel=\"stylesheet\" href=\"/styles.css\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo " - Day 04 FristSlim</title>
        ";
        // line 6
        $this->displayBlock('head', $context, $blocks);
        // line 7
        echo "    </head>
    <body>
        <div id=\"centeredContent\">
            <div id=\"content\">";
        // line 10
        $this->displayBlock('content', $context, $blocks);
        echo "</div>
            <div id=\"footer\">
                &copy; Copyright 2020 by <a href=\"http://day04slimfirst.ipd20/\"> JIAN</a>.
            </div>
        </div>
    </body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Default";
    }

    // line 6
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 10
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  81 => 10,  75 => 6,  68 => 5,  57 => 10,  52 => 7,  50 => 6,  46 => 5,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>        
        <link rel=\"stylesheet\" href=\"/styles.css\" />
        <title>{% block title %}Default{% endblock %} - Day 04 FristSlim</title>
        {% block head %}{% endblock %}
    </head>
    <body>
        <div id=\"centeredContent\">
            <div id=\"content\">{% block content %}{% endblock %}</div>
            <div id=\"footer\">
                &copy; Copyright 2020 by <a href=\"http://day04slimfirst.ipd20/\"> JIAN</a>.
            </div>
        </div>
    </body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\php\\day04slimfirst\\templates\\master.html.twig");
    }
}
