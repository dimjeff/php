<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#username").focusout(function(){
                if($("#username").val()!=""){
                    $.get("checkusername.php", { username:$("#username").val() }).done(function(data){
                        if(data!="true"){
                            //alert("This username has already been taken by another user.");
                            $("#isTaken").html("This username has already been taken by another user.");
                        }else{
                            $("#isTaken").html("");
                        }
                    });
                }
            });
        });
        $(document).ajaxError(function (event, jqxhr, settings, thrownError) {
            alert("Ajax error occured");
        });
    </script>
    <title>Document</title>
</head>
<body>
    <div class="form-style-10">
        <h1>
            New User Registration
        </h1>
        <?php
            require_once "db.php";

            function displayForm($username="", $email=""){
                $form = <<<EOD
                <form id="signup" method="post">
                <div class="section"><span>1</span>Username</div>
                <div class="inner-warp">
                    <label for="username">Username</label>
                    <input type="text" id="username" name="username" maxlength="50" required="required" value="$username"/><br/>
                    <span id="isTaken"></span>
                </div>
    
                <div class="section"><span>2</span>Email</div>
                <div class="inner-warp">
                    <label for="email">Email Address</label>
                    <input type="email" id="email" name="email" placeholder="yourname@xxx.com" required="required" value="$email"/>
                </div>
                <div class="section"><span>3</span>Password</div>
                <div class="inner-warp">
                    <label for="password">Password</label>
                    <input type="password" id="password" name="password" required="required"/>
                    <label for="confirm_password">Confirm Password</label>
                    <input type="password" id="confirm_password" name="confirm_password" required="required" />
                </div>
                <div class="divcenter">
                    <input type="submit" id="sign_up" name="sign_up" value="Register!"/>
                </div>
                </form>
EOD;

                echo $form;
            }

            if(isset($_POST['username'])){
                $username = $_POST['username'];
                $errors= array();
                if(preg_match('/^[a-z0-9]{4,20}$/',$username)!=1){
                    $errors[]='username must be 4-20 chars. including lower case letters and numbers';
                    $username = "";
                }
                $password = $_POST['password'];
                $confirmpwd = $_POST['confirm_password'];
                if($password!=$confirmpwd){
                    $errors[]='password is not same with confirm password.';
                    $password = "";
                    $confirmpwd = "";
                }

                if(preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/',$password)!=1){
                    $errors[]='Password must be at least 6 characters long and must contain at least one uppercase letter, one lower case letter, and one number or special character. It must not be longer than 10 characters.';
                    $password = "";
                    $confirmpwd = "";
                }

                $email = $_POST['email'];
                $result = file_get_contents("http://day02.ipd20/checkusername.php?username=$username");
                if($result != "true"){
                    $errors[]='This username has already been taken by another user.';
                    $username = "";
                }
                if(empty($errors)==true){                  
                    $sql = sprintf("INSERT INTO user(username,email,password) values('%s','%s','%s')",
                        mysqli_real_escape_string($conn,$username),
                        mysqli_real_escape_string($conn,$email),
                        mysqli_real_escape_string($conn,$password)
                    );
                    if(!mysqli_query($conn, $sql)){
                        echo "Failed to execute MySQL query:" . mysqli_error($conn);
                        exit();
                    }
                    echo "Successful, please to login.";
                    header("Location: login.php");
                    exit();
                }else{
                    echo "<ul>";
                    foreach($errors as $err)
                    {
                        echo "<li>$err</li>";
                    }
                    echo "</ul>";
                    displayForm($username, $email);
                }
            }else{
                displayForm();
            }
        ?>
    </div>
</body>
</html>