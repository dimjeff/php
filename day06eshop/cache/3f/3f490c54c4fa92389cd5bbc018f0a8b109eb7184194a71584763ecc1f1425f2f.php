<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_e989d30591416b76ed4979b3d2a20be296a126417f9db421860e5fb9a72a72ea extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "shop.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("shop.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Welcome to eShop - Home";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        if (($context["products"] ?? null)) {
            // line 7
            echo "    <div class=\"shop_content\">
        <div class=\"shop_bar clearfix\">
            <div class=\"shop_product_count\"><span>186</span> products found</div>
            <div class=\"shop_sorting\">
                <span>Sort by:</span>
                <ul>
                    <li>
                        <span class=\"sorting_text\">highest rated<i class=\"fas fa-chevron-down\"></span></i>
                        <ul>
                            <li class=\"shop_sorting_button\" data-isotope-option='{ \"sortBy\": \"original-order\" }'>highest rated</li>
                            <li class=\"shop_sorting_button\" data-isotope-option='{ \"sortBy\": \"name\" }'>name</li>
                            <li class=\"shop_sorting_button\"data-isotope-option='{ \"sortBy\": \"price\" }'>price</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        ";
            // line 24
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 25
                echo "            <div class=\"product_item is_new\">
                <div class=\"product_border\"></div>
                <div class=\"product_image d-flex flex-column align-items-center justify-content-center\">
                    ";
                // line 28
                if (twig_get_attribute($this->env, $this->source, $context["product"], "pictureFilePath", [], "any", false, false, false, 28)) {
                    echo "<img src=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "pictureFilePath", [], "any", false, false, false, 28), "html", null, true);
                    echo "\" alt=\"\">";
                }
                // line 29
                echo "                </div>
                <div class=\"product_content\">
                    <div class=\"product_price\">";
                // line 31
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "unitPrice", [], "any", false, false, false, 31), "html", null, true);
                echo "</div>
                    <div class=\"product_name\"><div><a href=\"#\" tabindex=\"0\">";
                // line 32
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 32), "html", null, true);
                echo "</a></div></div>
                </div>
                <div class=\"product_fav\"><i class=\"fas fa-heart\"></i></div>
                <ul class=\"product_marks\">
                    <li class=\"product_mark product_discount\">-25%</li>
                    <li class=\"product_mark product_new\">new</li>
                </ul>
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 41
            echo "
        ";
            // line 42
            if (($context["totalpages"] ?? null)) {
                // line 43
                echo "            <div class=\"shop_page_nav d-flex flex-row\">
                <div class=\"page_prev d-flex flex-column align-items-center justify-content-center\"><i class=\"fas fa-chevron-left\"></i></div>
                <ul class=\"page_nav d-flex flex-row\">
                    ";
                // line 46
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, ($context["totalpages"] ?? null)));
                foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                    // line 47
                    echo "                    <li><a href=\"#\">";
                    echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                    echo "</a></li>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 49
                echo "                </ul>
                <div class=\"page_next d-flex flex-column align-items-center justify-content-center\"><i class=\"fas fa-chevron-right\"></i></div>
            </div>
        ";
            }
            // line 53
            echo "    </div>
    ";
        }
        // line 54
        echo " 

    
";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 54,  147 => 53,  141 => 49,  132 => 47,  128 => 46,  123 => 43,  121 => 42,  118 => 41,  103 => 32,  99 => 31,  95 => 29,  89 => 28,  84 => 25,  80 => 24,  61 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"shop.html.twig\" %}

{% block title %}Welcome to eShop - Home{% endblock %}

{% block content %}
    {% if products %}
    <div class=\"shop_content\">
        <div class=\"shop_bar clearfix\">
            <div class=\"shop_product_count\"><span>186</span> products found</div>
            <div class=\"shop_sorting\">
                <span>Sort by:</span>
                <ul>
                    <li>
                        <span class=\"sorting_text\">highest rated<i class=\"fas fa-chevron-down\"></span></i>
                        <ul>
                            <li class=\"shop_sorting_button\" data-isotope-option='{ \"sortBy\": \"original-order\" }'>highest rated</li>
                            <li class=\"shop_sorting_button\" data-isotope-option='{ \"sortBy\": \"name\" }'>name</li>
                            <li class=\"shop_sorting_button\"data-isotope-option='{ \"sortBy\": \"price\" }'>price</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        {% for product in products %}
            <div class=\"product_item is_new\">
                <div class=\"product_border\"></div>
                <div class=\"product_image d-flex flex-column align-items-center justify-content-center\">
                    {%if product.pictureFilePath %}<img src=\"{{product.pictureFilePath}}\" alt=\"\">{% endif %}
                </div>
                <div class=\"product_content\">
                    <div class=\"product_price\">{{product.unitPrice}}</div>
                    <div class=\"product_name\"><div><a href=\"#\" tabindex=\"0\">{{product.name}}</a></div></div>
                </div>
                <div class=\"product_fav\"><i class=\"fas fa-heart\"></i></div>
                <ul class=\"product_marks\">
                    <li class=\"product_mark product_discount\">-25%</li>
                    <li class=\"product_mark product_new\">new</li>
                </ul>
            </div>
        {% endfor %}

        {% if totalpages %}
            <div class=\"shop_page_nav d-flex flex-row\">
                <div class=\"page_prev d-flex flex-column align-items-center justify-content-center\"><i class=\"fas fa-chevron-left\"></i></div>
                <ul class=\"page_nav d-flex flex-row\">
                    {% for page in 1..totalpages %}
                    <li><a href=\"#\">{{ page }}</a></li>
                    {% endfor %}
                </ul>
                <div class=\"page_next d-flex flex-column align-items-center justify-content-center\"><i class=\"fas fa-chevron-right\"></i></div>
            </div>
        {% endif %}
    </div>
    {% endif %} 

    
{% endblock %}", "index.html.twig", "C:\\xampp\\htdocs\\php\\day06eshop\\templates\\index.html.twig");
    }
}
